#!/bin/bash

# Clean the original model
rm -f Model/fitness.out
rm -f Model/norms/outputs/*

# Remove all copied Models
for (( i=0; i<$1; i++ ))
do
	rm -rf Model$i
done
