#!/bin/bash

# Clean the original model
rm -f Model/fitness.out
rm -f Model/norms/outputs/*

# Create new copies
for (( i=0; i<$1; i++ ))
do
	cp -r Model Model$i
done
