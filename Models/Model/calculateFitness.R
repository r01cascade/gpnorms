CORES <- 1
N_YEARS <- 15
TARGET_YEARS <- c(T, F, F, T, F, F, T, F, F, T, F,  # 1979 to 1989
                  T, T, T, T, T, T, T, T, T, T,    # 1990 to 1999
                  T, T, T, T, T, T, T, T, T, T,  # 2000 to 2009 
                  T, T, T, T, T, T, T) # years 2010 to 2016 
TARGET_YEARS <- TARGET_YEARS[1:(N_YEARS+1)]  ## WARNIMG: Added +1 to deal with changes in data output after core was changed
N_TARGET_YEARS <- length(TARGET_YEARS[which(TARGET_YEARS == T)])
calTargets <- c("PREV_SEX1P", "PREV_SEX2P", "FREQ_SEX1P", "FREQ_SEX2P", "QUANT_SEX1P", "QUANT_SEX2P")
seTargets <- c("PREV_SEX1SE", "PREV_SEX2SE", "FREQ_SEX1SE", "FREQ_SEX2SE", "QUANT_SEX1SE", "QUANT_SEX2SE")

# Set up the stem of the working directory and filepaths
#dirname(parent.frame(2)$ofile)
#dirname(sys.frame(1)$ofile)
#this.dir <- "/home/tuong/GPNorms/Models/Model"
this.dir <- system("pwd", intern = T)


#######################################
# Postprocess outputs so that they are in the same format as the target data
#######################################

simout <- read.csv(paste(this.dir, "/norms/outputs/annual_data.csv", sep = ""), header = TRUE)

# prevalence of current drinking by sex and by age group
simout$PREV_SEX1P <- ifelse(simout$Male > 0 , simout$X12MonthDrinkersMale/simout$Male, 0)
simout$PREV_SEX2P <- ifelse(simout$Female > 0 , simout$X12MonthDrinkersFemale/simout$Female, 0)
simout$PREV_AGECAT1P <- ifelse(simout$AgeGroup1 > 0 ,simout$X12MonthDrinkersAgeGroup1/simout$AgeGroup1, 0)
simout$PREV_AGECAT2P <- ifelse(simout$AgeGroup2 > 0 ,simout$X12MonthDrinkersAgeGroup2/simout$AgeGroup2, 0)
simout$PREV_AGECAT3P <- ifelse(simout$AgeGroup3 > 0 ,simout$X12MonthDrinkersAgeGroup3/simout$AgeGroup3, 0)
simout$PREV_AGECAT4P <- ifelse(simout$AgeGroup4 > 0 ,simout$X12MonthDrinkersAgeGroup4/simout$AgeGroup4, 0)

# average quantity among drinkers in grams per day
simout$QUANT_SEX1P <- ifelse(simout$X12MonthDrinkersMale > 0 , (simout$QuantMale*14)/simout$X12MonthDrinkersMale, 0)
simout$QUANT_SEX2P <- ifelse(simout$X12MonthDrinkersFemale > 0 , (simout$QuantFemale*14)/simout$X12MonthDrinkersFemale, 0)
simout$QUANT_AGECAT1P <- ifelse(simout$X12MonthDrinkersAgeGroup1 > 0 , (simout$QuantAgeGroup1*14)/simout$X12MonthDrinkersAgeGroup1, 0)
simout$QUANT_AGECAT2P <- ifelse(simout$X12MonthDrinkersAgeGroup2 > 0 , (simout$QuantAgeGroup2*14)/simout$X12MonthDrinkersAgeGroup2, 0)
simout$QUANT_AGECAT3P <- ifelse(simout$X12MonthDrinkersAgeGroup3 > 0 , (simout$QuantAgeGroup3*14)/simout$X12MonthDrinkersAgeGroup3, 0)
simout$QUANT_AGECAT4P <- ifelse(simout$X12MonthDrinkersAgeGroup4 > 0 , (simout$QuantAgeGroup4*14)/simout$X12MonthDrinkersAgeGroup4, 0)

# average frequency
simout$FREQ_SEX1P  <- ifelse(simout$X12MonthDrinkersMale > 0 , simout$FreqMale / simout$X12MonthDrinkersMale, 0)
simout$FREQ_SEX2P  <- ifelse(simout$X12MonthDrinkersFemale > 0 , simout$FreqFemale / simout$X12MonthDrinkersFemale, 0)
simout$FREQ_AGECAT1P  <- ifelse(simout$X12MonthDrinkersAgeGroup1 > 0 , simout$FreqAgeGroup1 / simout$X12MonthDrinkersAgeGroup1, 0)
simout$FREQ_AGECAT2P  <- ifelse(simout$X12MonthDrinkersAgeGroup2 > 0 , simout$FreqAgeGroup2 / simout$X12MonthDrinkersAgeGroup2, 0)
simout$FREQ_AGECAT3P  <- ifelse(simout$X12MonthDrinkersAgeGroup3 > 0 , simout$FreqAgeGroup3 / simout$X12MonthDrinkersAgeGroup3, 0)
simout$FREQ_AGECAT4P  <- ifelse(simout$X12MonthDrinkersAgeGroup4 > 0 , simout$FreqAgeGroup4 / simout$X12MonthDrinkersAgeGroup4, 0)

# heavy episodic drinking days
simout$HED_TOTALP <- ifelse(simout$X12MonthDrinkers > 0, simout$SumOccasionalHeavyDrinking / simout$X12MonthDrinkers, 0)

simout <- simout[, names(simout) %in% calTargets]
# order outTargets according to caltargets 
simout <- simout[,calTargets]

d <- dim(simout)

#######################################
# load targets
#######################################

targets <- read.csv("./target_NHSDA.csv")

#separate Mean and SE
meancols <- which(unlist(lapply(names(targets), function(x) substring(x, nchar(x)))) == "P")
secols <- which(unlist(lapply(names(targets), function(x) substring(x, nchar(x)))) == "E")

targetMeans <- targets[, meancols]
targetSE <- targets[, secols]

targetMeans <- targetMeans[1:d[1],]
targetSE <- targetSE[1:d[1],]

targetMeans <- targetMeans[,calTargets]
targetSE <- targetSE[,seTargets]

#######################################
# calculate fitness
#######################################

#don't include 1979
simout <- simout[2:d[1],]
targetMeans <- targetMeans[2:d[1],]
targetSE <- targetSE[2:d[1],]

d <- dim(simout)

rangeMin <- array(c(rep(0,d[1]*2),rep(1,d[1]*2),rep(0,d[1]*2)),dim = dim(simout))
rangeMax <- array(c(rep(1,d[1]*2),rep(7,d[1]*2),rep(30,d[1]*2)),dim = dim(simout))

rangePercentage <- 0.10
implausibility <- array(dim = dim(simout))
denoms <-  sqrt( targetSE^2 + (rangePercentage*(rangeMax-rangeMin))^2 )
implausibility <- abs(targetMeans - simout) / denoms
implausibility <- as.matrix(implausibility)

fitness <- mean(implausibility, na.rm = TRUE)
write(fitness, file="fitness.out")
