/*
 * globals.cpp
 *
 *  Created on: 2017-10-17
 *      Author: lotta
 */

#include "globals.h"


const bool MALE = true; // = 1
const bool FEMALE = false; // = 0
const int NUM_SEX = 2;
const int NUM_AGE_GROUPS = 9;
const int MIN_AGE = 12;
const int MAX_AGE = 100;
const int MAX_DRINKS = 30;
const int AGE_GROUPS[NUM_AGE_GROUPS] =
		{ 14, 24, 34, 44, 54, 64, 74, 84, MAX_AGE };

const int BIRTH_RATE_BASE = 10;
double BIRTH_RATES[30] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

//20 years (1980-1999). Rates of 2009,2010 is copied from 2008
double MORTALITY_RATES[20][2][9] = {
				{{8.24E-05,8.24E-05,1.06E-04,2.07E-04,3.33E-04,5.08E-04,9.40E-04,1.21E-03,7.22E-04},{2.87E-05,2.87E-05,4.82E-05,1.15E-04,1.98E-04,3.39E-04,7.24E-04,1.26E-03,1.50E-03}},
				{{8.55E-05,8.55E-05,1.16E-04,2.13E-04,3.32E-04,5.09E-04,9.68E-04,1.21E-03,7.12E-04},{3.02E-05,3.02E-05,5.07E-05,1.15E-04,1.99E-04,3.42E-04,7.35E-04,1.25E-03,1.47E-03}},
				{{9.03E-05,9.03E-05,1.34E-04,2.39E-04,3.37E-04,5.24E-04,9.99E-04,1.21E-03,6.97E-04},{3.02E-05,3.02E-05,5.60E-05,1.18E-04,1.98E-04,3.44E-04,7.60E-04,1.25E-03,1.44E-03}},
				{{9.68E-05,9.68E-05,1.57E-04,2.63E-04,3.41E-04,5.34E-04,1.03E-03,1.20E-03,6.87E-04},{3.18E-05,3.18E-05,5.98E-05,1.21E-04,1.96E-04,3.50E-04,7.77E-04,1.25E-03,1.42E-03}},
				{{1.02E-04,1.02E-04,1.64E-04,2.61E-04,3.32E-04,5.48E-04,1.05E-03,1.19E-03,6.69E-04},{3.22E-05,3.22E-05,6.12E-05,1.17E-04,1.92E-04,3.53E-04,7.88E-04,1.24E-03,1.38E-03}},
				{{1.04E-04,1.04E-04,1.68E-04,2.55E-04,3.21E-04,5.65E-04,1.08E-03,1.21E-03,6.65E-04},{3.30E-05,3.30E-05,6.14E-05,1.14E-04,1.86E-04,3.64E-04,8.01E-04,1.25E-03,1.37E-03}},
				{{1.02E-04,1.02E-04,1.67E-04,2.46E-04,3.09E-04,5.73E-04,1.07E-03,1.17E-03,6.29E-04},{3.25E-05,3.25E-05,6.08E-05,1.10E-04,1.78E-04,3.67E-04,7.94E-04,1.21E-03,1.27E-03}},
				{{1.09E-04,1.09E-04,1.73E-04,2.39E-04,3.01E-04,5.99E-04,1.09E-03,1.18E-03,6.20E-04},{3.52E-05,3.52E-05,6.29E-05,1.09E-04,1.76E-04,3.82E-04,8.02E-04,1.22E-03,1.27E-03}},
				{{1.12E-04,1.12E-04,1.76E-04,2.30E-04,2.99E-04,6.20E-04,1.11E-03,1.18E-03,6.09E-04},{3.55E-05,3.55E-05,6.42E-05,1.05E-04,1.76E-04,3.93E-04,8.09E-04,1.23E-03,1.25E-03}},
				{{1.10E-04,1.10E-04,1.79E-04,2.24E-04,3.03E-04,6.47E-04,1.13E-03,1.19E-03,6.07E-04},{3.78E-05,3.78E-05,6.63E-05,1.04E-04,1.76E-04,4.07E-04,8.22E-04,1.24E-03,1.25E-03}},
				{{1.17E-04,1.17E-04,1.76E-04,2.14E-04,3.02E-04,6.76E-04,1.16E-03,1.21E-03,6.20E-04},{3.94E-05,3.94E-05,6.61E-05,1.02E-04,1.78E-04,4.27E-04,8.37E-04,1.26E-03,1.26E-03}},
				{{1.17E-04,1.17E-04,1.72E-04,2.04E-04,3.01E-04,6.95E-04,1.17E-03,1.18E-03,6.00E-04},{4.04E-05,4.04E-05,6.59E-05,9.95E-05,1.78E-04,4.33E-04,8.38E-04,1.23E-03,1.21E-03}},
				{{1.24E-04,1.24E-04,1.72E-04,1.95E-04,3.01E-04,7.18E-04,1.18E-03,1.18E-03,5.95E-04},{4.20E-05,4.20E-05,6.37E-05,9.77E-05,1.78E-04,4.44E-04,8.43E-04,1.22E-03,1.18E-03}},
				{{1.18E-04,1.18E-04,1.57E-04,1.83E-04,3.08E-04,7.47E-04,1.19E-03,1.18E-03,5.95E-04},{4.11E-05,4.11E-05,6.09E-05,9.38E-05,1.82E-04,4.57E-04,8.39E-04,1.21E-03,1.17E-03}},
				{{1.22E-04,1.22E-04,1.52E-04,1.72E-04,3.13E-04,7.57E-04,1.18E-03,1.15E-03,5.80E-04},{4.31E-05,4.31E-05,6.01E-05,9.32E-05,1.84E-04,4.61E-04,8.36E-04,1.19E-03,1.11E-03}},
				{{1.23E-04,1.23E-04,1.49E-04,1.64E-04,3.23E-04,7.69E-04,1.20E-03,1.14E-03,5.76E-04},{4.38E-05,4.38E-05,5.96E-05,8.97E-05,1.90E-04,4.67E-04,8.33E-04,1.17E-03,1.09E-03}},
				{{1.35E-04,1.35E-04,1.53E-04,1.63E-04,3.37E-04,7.74E-04,1.19E-03,1.10E-03,5.55E-04},{4.57E-05,4.57E-05,6.02E-05,8.91E-05,1.95E-04,4.65E-04,8.22E-04,1.15E-03,1.03E-03}},
				{{1.47E-04,1.47E-04,1.60E-04,1.65E-04,3.57E-04,7.93E-04,1.20E-03,1.10E-03,5.57E-04},{4.97E-05,4.97E-05,6.33E-05,8.99E-05,2.06E-04,4.71E-04,8.24E-04,1.14E-03,1.02E-03}},
				{{1.62E-04,1.62E-04,1.59E-04,1.66E-04,3.72E-04,8.11E-04,1.22E-03,1.11E-03,5.64E-04},{5.34E-05,5.34E-05,6.25E-05,9.16E-05,2.14E-04,4.75E-04,8.33E-04,1.16E-03,1.01E-03}}
};

int INJUNCTIVE_THRESHOLD = 999; // this is read in from the model.props
double INJUNCTIVE_PROPORTION = 99.9; // this is read in from the model.props
double INJUNCTIVE_ADJUSTMENT = 99.9; // this is read in from the model.props
double INJ_RELAXATION_GAMMA_ADJUSTMENT = 1.05; // this is read in from the model.props
double INJ_RELAXATION_LAMBDA_ADJUSTMENT = 0.95; // this is read in from the model.props
double INJ_PUNISHMENT_GAMMA_ADJUSTMENT = 0.95; // this is read in from the model.props
double INJ_PUNISHMENT_LAMBDA_ADJUSTMENT = 1.05; // this is read in from the model.props

// Todo: this needs to be specified for the descriptive entity
const int DESCRIPTIVE_INCUBATION_PERIOD = 100;
const double DESCRIPTIVE_INCUBATION_PERCENT = 0.85;

// This is the bias in perceiving quantity
double PERCEPTION_BIAS = 0.5; // this is read in from model.props

// Time periods over which drinking behaviour it is evaluated
int N_DAYS_DESCRIPTIVE = 30; // is used to generate descriptive norms on prevalence and average quantity
int COMP_DAYS_PUNISH = 90; // Used in the binge punishment (average drinks in comp days > injunctive threshold)
int COMP_DAYS_RELAX = 90; // Used in the injunctive relaxation (prevalence of one drink over comp days)

const double DEFAULT_D_NORM = 0.5;
const double DEFAULT_D_NORM_QUANT = 0.5;
const double DEFAULT_D_NORM_QUANT_SD = 0.1;

const int MAX_DRINK_LEVEL = 7;
const int MIN_DRINK_LEVEL = 1;

// CP Edit: add the discounting factor to decrease payoff with increasing drinks
double DISCOUNT_MALE = 0.1;
double DISCOUNT_FEMALE = 0.1;

double DESIRE_MULTIPLIER = 1.0;

boost::unordered_map<int, std::pair<double, double> > MEAN_SD_LOOKUP_TABLE;
