/* Model.cpp */

#include <stdio.h>
#include <vector>
#include <boost/mpi.hpp>
#include <istream>
#include <string>
#include <cmath>

#include "repast_hpc/AgentId.h"
#include "repast_hpc/RepastProcess.h"
#include "repast_hpc/Utilities.h"
#include "repast_hpc/Properties.h"

#include "Model.h"
#include "globals.h"

AgentPackageProvider::AgentPackageProvider(repast::SharedContext<Agent>* agentPtr): agents(agentPtr){ }

void AgentPackageProvider::providePackage(Agent * agent, std::vector<AgentPackage>& out){
    repast::AgentId id = agent->getId();
    AgentPackage package(id.id(), id.startingRank(), id.agentType(), id.currentRank(), agent->getAge(), agent->getSex(), agent->isDrinkingToday(),
    	                 agent->getNumberDrinksToday(), agent->getDrinkFrequencyLevel(), agent->getPastYearDrinks());
    out.push_back(package);
}

void AgentPackageProvider::provideContent(repast::AgentRequest req, std::vector<AgentPackage>& out){
    std::vector<repast::AgentId> ids = req.requestedAgents();
    for(size_t i = 0; i < ids.size(); i++){
        providePackage(agents->getAgent(ids[i]), out);
    }
}

AgentPackageReceiver::AgentPackageReceiver(repast::SharedContext<Agent>* agentPtr): agents(agentPtr){}

Agent * AgentPackageReceiver::createAgent(AgentPackage package){
    repast::AgentId id(package.id, package.rank, package.type, package.currentRank);
    return new Agent(id); // When agent moves to other process he/she only takes ID; all other properties will be reset
}

void AgentPackageReceiver::updateAgent(AgentPackage package){
    repast::AgentId id(package.id, package.rank, package.type);
    Agent * agent = agents->getAgent(id);
    agent->set(package.currentRank, package.age, package.sex, package.isDrinkingToday, package.numberDrinksToday,
    		   package.drinkFrequencyLevel, package.pastYearDrinks);
}


Model::Model(std::string propsFile, int argc, char** argv, boost::mpi::communicator* comm): context(comm){
	props = new repast::Properties(propsFile, argc, argv, comm);
	stopAt = repast::strToInt(props->getProperty("stop.at"));
	countOfAgents = repast::strToInt(props->getProperty("count.of.agents"));
	simYear = 0;

	// Read in all-cause mortality from model.props file
	std::vector<std::string> mortality_data;
	repast::tokenize(props->getProperty("microsim.mortality.data"), mortality_data, ",");

	//init NUM_SEX * NUM_AGE_GROUPS vector of 0.0
	std::vector<std::vector<double>> oneYearMortality;
	for (int i=0; i<NUM_SEX; i++) {
		std::vector<double> tempRow;
		for (int j=0; j<NUM_AGE_GROUPS; j++) {
			tempRow.push_back(0.0);
		}
		oneYearMortality.push_back(tempRow);
	}

	std::vector<std::string>::const_iterator mortality_iter = mortality_data.begin();
	int current_year = 0;
	int current_row = 0;
	int current_col = 0;
	while (mortality_iter != mortality_data.end()){
		oneYearMortality[current_row][current_col] = repast::strToDouble(*mortality_iter);
		current_col++;
		if (current_col > (NUM_AGE_GROUPS - 1)) {
			current_row++;
			current_col = 0;
		}
		if (current_row > (NUM_SEX - 1)) {
			current_year++;
			current_row = 0;
			current_col = 0;
			mortalityRates.push(oneYearMortality);
		}
		mortality_iter++;
	}

	// Read birth rates
	std::vector<std::string> birthRates;
	repast::tokenize(props->getProperty("microsim.birthrate"), birthRates, ",");
	std::vector<std::string>::const_iterator births_iter = birthRates.begin();
	current_col = 0;
	while (births_iter != birthRates.end()){
		BIRTH_RATES[current_col] = repast::strToDouble(*births_iter);
		current_col++;
		births_iter++;
	}
	//if(repast::RepastProcess::instance()->rank() == 0) props->writeToSVFile("./outputs/record.csv");
	provider = new AgentPackageProvider(&context);
	receiver = new AgentPackageReceiver(&context);
	startTime = std::chrono::steady_clock::now();

	//Read intervals for 3 mechanisms
	ACTION_MECHANISM_INTERVAL_TICK = repast::strToInt(props->getProperty("action.interval"));
	SITUATIONAL_MECHANISM_INTERVAL_TICK = repast::strToInt(props->getProperty("situational.interval"));

	//read in ERFC lookup table
	std::string lookupFilename = props->getProperty("compressed.lookup.file");
	readMeanSDLookupTable(lookupFilename);
}

void Model::readMeanSDLookupTable(std::string lookupFilename) {

	//read from csv and store in localTable
	std::queue<std::vector<std::string>> localTable;

	ifstream myfile(lookupFilename);
	if (myfile.is_open()) {
		//read the csv file
		localTable = readCSV(myfile);
		myfile.close();
	} else {
		std::cerr << "Unable to open lookup file: " << lookupFilename << std::endl;
	}

	//find index on header line.
	int indexKey = -1;
	int indexMean = -1;
	int indexSd = -1;

	std::vector<string> headerLine = localTable.front();
	localTable.pop();
	for (int i = 0; i < headerLine.size(); ++i){
		if (headerLine[i]=="hashkey")
			indexKey = i;
		if (headerLine[i]=="ERFC.mean")
			indexMean = i;
		if (headerLine[i]=="ERFC.SD")
			indexSd = i;
	}
	if (indexKey==-1 || indexMean ==-1 || indexSd==-1)
		std::cerr << "Index for ERFC not found" << std::endl;

	//read in variables and create map stored in globals.
	int localKey;
	double localMean;
	double localSd;
	std::pair<double, double> localMeanSd;

	//read each line and put into a global table
	while (!localTable.empty()) {
		std::vector<string> localMap = localTable.front();
		localTable.pop();

		//read the variables from a row of localTable
		localKey = repast::strToInt(localMap[indexKey]);
		localMean = repast::strToDouble(localMap[indexMean]);
		localSd = repast::strToDouble(localMap[indexSd]);
		localMeanSd = std::make_pair(localMean, localSd);
		//create a key/pair map from row.

		MEAN_SD_LOOKUP_TABLE.insert(std::make_pair(localKey, localMeanSd));

	}

}

Model::~Model(){
	delete props;
	delete provider;
	delete receiver;
	delete annualValues;
	for (std::vector<StructuralEntity*>::iterator it=structuralEntityList.begin(); it!= structuralEntityList.end(); ++it)
		delete (*it);
}

// Generating agents, theories, structural entities
void Model::initAgents(){
	double monthlyDrinksSDPct = repast::strToDouble(props->getProperty("temp.monthly.sd.pct"));

	//find index from the header line, ASSUMING the infoTable was already be read
	mIndexId = -1;
	mIndexSex = -1;
	mIndexAge = -1;
	mIndexDrinking = -1;
	mIndexFrequencyLevel = -1;
	mIndexMonthlyDrinks = -1;
	std::vector<string> headerLine = infoTable.front();
	infoTable.pop();
	for (int i = 0; i < headerLine.size(); ++i) {
		if ( headerLine[i]=="microsim.init.id" )
			mIndexId = i;
		if ( headerLine[i]=="microsim.init.sex" )
			mIndexSex = i;
		if ( headerLine[i]=="microsim.init.age" )
			mIndexAge = i;
		if ( headerLine[i]=="microsim.init.drinkingstatus" )
			mIndexDrinking = i;
		if ( headerLine[i]=="temp.drink.frequency" )
			mIndexFrequencyLevel = i;
		if ( headerLine[i]=="temp.drinks.per.month" )
			mIndexMonthlyDrinks = i;
	}
	if (mIndexId==-1 || mIndexSex==-1 || mIndexAge==-1 ||
			mIndexDrinking==-1 || mIndexFrequencyLevel==-1 || mIndexMonthlyDrinks==-1 )
		std::cerr << "Index not found" << std::endl;

	//read in variables and create agents
	int tempId;
	bool sex;
	int age;
	int drinking;
	int drinkFrequencyLevel;
	int monthlyDrinks;
	//ignore first line, and assume the rest of lines = countOfAgents in model.props
	while (!infoTable.empty() && context.size() < countOfAgents) {
		std::vector<string> info = infoTable.front();
		infoTable.pop();

		//read the variables from a row of infoTable
		tempId = repast::strToInt(info[mIndexId]);
		sex = repast::strToInt(info[mIndexSex]);
		age = repast::strToInt(info[mIndexAge]);
		drinking = repast::strToInt(info[mIndexDrinking]);
		drinkFrequencyLevel = repast::strToInt(info[mIndexFrequencyLevel]);
		monthlyDrinks = repast::strToInt(info[mIndexMonthlyDrinks]);

		//create an agent using a constructor with variables from file
		int rank = repast::RepastProcess::instance()->rank();
		repast::AgentId id(tempId, rank, 0);
		id.currentRank(rank);
		Agent *agent = new Agent(id, sex, age, drinking,
				drinkFrequencyLevel, monthlyDrinks, monthlyDrinksSDPct);
		context.addAgent(agent);

		//init mediators and theory
		initMediatorAndTheoryFromFile(agent, info);
	}

	//Init agents complete. Overwrite infoTable with spawn file data
	getReadyToSpawn();

	//Annual data collection
	std::string fileOutputName("./outputs/annual_data.csv");
	repast::SVDataSetBuilder builder(fileOutputName.c_str(), ",", repast::RepastProcess::instance()->getScheduleRunner().schedule());

	OutSumPopulation* outSumPopulation = new OutSumPopulation(&context);
	builder.addDataSource(repast::createSVDataSource("Population", outSumPopulation, std::plus<int>()));

	OutSumMale* outSumMale = new OutSumMale(&context);
	builder.addDataSource(repast::createSVDataSource("Male", outSumMale, std::plus<int>()));

	OutSumFemale* outSumFemale = new OutSumFemale(&context);
	builder.addDataSource(repast::createSVDataSource("Female", outSumFemale, std::plus<int>()));

	OutSumAgeGroup1* outSumAgeGroup1 = new OutSumAgeGroup1(&context);
	builder.addDataSource(repast::createSVDataSource("AgeGroup1", outSumAgeGroup1, std::plus<int>()));

	OutSumAgeGroup2* outSumAgeGroup2 = new OutSumAgeGroup2(&context);
	builder.addDataSource(repast::createSVDataSource("AgeGroup2", outSumAgeGroup2, std::plus<int>()));

	OutSumAgeGroup3* outSumAgeGroup3 = new OutSumAgeGroup3(&context);
	builder.addDataSource(repast::createSVDataSource("AgeGroup3", outSumAgeGroup3, std::plus<int>()));

	OutSumAgeGroup4* outSumAgeGroup4 = new OutSumAgeGroup4(&context);
	builder.addDataSource(repast::createSVDataSource("AgeGroup4", outSumAgeGroup4, std::plus<int>()));


	OutSum12MonthDrinkers* outSum12MonthDrinkers = new OutSum12MonthDrinkers(&context);
	builder.addDataSource(repast::createSVDataSource("12MonthDrinkers", outSum12MonthDrinkers, std::plus<int>()));

	OutSum12MonthDrinkersMale* outSum12MonthDrinkersMale = new OutSum12MonthDrinkersMale(&context);
	builder.addDataSource(repast::createSVDataSource("12MonthDrinkersMale", outSum12MonthDrinkersMale, std::plus<int>()));

	OutSum12MonthDrinkersFemale* outSum12MonthDrinkersFemale = new OutSum12MonthDrinkersFemale(&context);
	builder.addDataSource(repast::createSVDataSource("12MonthDrinkersFemale", outSum12MonthDrinkersFemale, std::plus<int>()));

	OutSum12MonthDrinkersAgeGroup1* outSum12MonthDrinkersAgeGroup1 = new OutSum12MonthDrinkersAgeGroup1(&context);
	builder.addDataSource(repast::createSVDataSource("12MonthDrinkersAgeGroup1", outSum12MonthDrinkersAgeGroup1, std::plus<int>()));

	OutSum12MonthDrinkersAgeGroup2* outSum12MonthDrinkersAgeGroup2 = new OutSum12MonthDrinkersAgeGroup2(&context);
	builder.addDataSource(repast::createSVDataSource("12MonthDrinkersAgeGroup2", outSum12MonthDrinkersAgeGroup2, std::plus<int>()));

	OutSum12MonthDrinkersAgeGroup3* outSum12MonthDrinkersAgeGroup3 = new OutSum12MonthDrinkersAgeGroup3(&context);
	builder.addDataSource(repast::createSVDataSource("12MonthDrinkersAgeGroup3", outSum12MonthDrinkersAgeGroup3, std::plus<int>()));

	OutSum12MonthDrinkersAgeGroup4* outSum12MonthDrinkersAgeGroup4 = new OutSum12MonthDrinkersAgeGroup4(&context);
	builder.addDataSource(repast::createSVDataSource("12MonthDrinkersAgeGroup4", outSum12MonthDrinkersAgeGroup4, std::plus<int>()));


	OutSumQuantMale* outSumQuantMale = new OutSumQuantMale(&context);
	builder.addDataSource(repast::createSVDataSource("QuantMale", outSumQuantMale, std::plus<double>()));

	OutSumQuantFemale* outSumQuantFemale = new OutSumQuantFemale(&context);
	builder.addDataSource(repast::createSVDataSource("QuantFemale", outSumQuantFemale, std::plus<double>()));

	OutSumQuantAgeGroup1* outSumQuantAgeGroup1 = new OutSumQuantAgeGroup1(&context);
	builder.addDataSource(repast::createSVDataSource("QuantAgeGroup1", outSumQuantAgeGroup1, std::plus<double>()));

	OutSumQuantAgeGroup2* outSumQuantAgeGroup2 = new OutSumQuantAgeGroup2(&context);
	builder.addDataSource(repast::createSVDataSource("QuantAgeGroup2", outSumQuantAgeGroup2, std::plus<double>()));

	OutSumQuantAgeGroup3* outSumQuantAgeGroup3 = new OutSumQuantAgeGroup3(&context);
	builder.addDataSource(repast::createSVDataSource("QuantAgeGroup3", outSumQuantAgeGroup3, std::plus<double>()));

	OutSumQuantAgeGroup4* outSumQuantAgeGroup4 = new OutSumQuantAgeGroup4(&context);
	builder.addDataSource(repast::createSVDataSource("QuantAgeGroup4", outSumQuantAgeGroup4, std::plus<double>()));


	OutSumFreqMale* outSumFreqMale = new OutSumFreqMale(&context);
	builder.addDataSource(repast::createSVDataSource("FreqMale", outSumFreqMale, std::plus<int>()));

	OutSumFreqFemale* outSumFreqFemale = new OutSumFreqFemale(&context);
	builder.addDataSource(repast::createSVDataSource("FreqFemale", outSumFreqFemale, std::plus<int>()));

	OutSumFreqAgeGroup1* outSumFreqAgeGroup1 = new OutSumFreqAgeGroup1(&context);
	builder.addDataSource(repast::createSVDataSource("FreqAgeGroup1", outSumFreqAgeGroup1, std::plus<int>()));

	OutSumFreqAgeGroup2* outSumFreqAgeGroup2 = new OutSumFreqAgeGroup2(&context);
	builder.addDataSource(repast::createSVDataSource("FreqAgeGroup2", outSumFreqAgeGroup2, std::plus<int>()));

	OutSumFreqAgeGroup3* outSumFreqAgeGroup3 = new OutSumFreqAgeGroup3(&context);
	builder.addDataSource(repast::createSVDataSource("FreqAgeGroup3", outSumFreqAgeGroup3, std::plus<int>()));

	OutSumFreqAgeGroup4* outSumFreqAgeGroup4 = new OutSumFreqAgeGroup4(&context);
	builder.addDataSource(repast::createSVDataSource("FreqAgeGroup4", outSumFreqAgeGroup4, std::plus<int>()));


	OutSumOccasionalHeavyDrinking* outSumOccasionalHeavyDrinking = new OutSumOccasionalHeavyDrinking(&context);
	builder.addDataSource(repast::createSVDataSource("SumOccasionalHeavyDrinking", outSumOccasionalHeavyDrinking, std::plus<int>()));

	annualValues = builder.createDataSet();

	initNetwork();
}

void Model::getReadyToSpawn() {
	//read in file for spawning info table
	int rank = repast::RepastProcess::instance()->rank();
	std::string rankFileNameProperty = "spawn.file.rank" + std::to_string(rank);
	std::string rankFileName = props->getProperty(rankFileNameProperty);
	std::string spawnFileName = rankFileName;
	readRankFileForTheory(spawnFileName);

	//find index from the header line, ASSUMING the infoTable was already be read
	mIndexId = -1;
	mIndexSex = -1;
	mIndexAge = -1;
	mIndexDrinking = -1;
	mIndexFrequencyLevel = -1;
	mIndexMonthlyDrinks = -1;
	mIndexSpawnTick = -1;
	std::vector<string> headerLine = infoTable.front();
	infoTable.pop();
	for (int i = 0; i < headerLine.size(); ++i) {
		if ( headerLine[i]=="microsim.init.id" )
			mIndexId = i;
		if ( headerLine[i]=="microsim.init.sex" )
			mIndexSex = i;
		if ( headerLine[i]=="microsim.init.age" )
			mIndexAge = i;
		if ( headerLine[i]=="microsim.init.drinkingstatus" )
			mIndexDrinking = i;
		if ( headerLine[i]=="temp.drink.frequency" )
			mIndexFrequencyLevel = i;
		if ( headerLine[i]=="temp.drinks.per.month" )
			mIndexMonthlyDrinks = i;
		if ( headerLine[i]=="microsim.spawn.tick" )
			mIndexSpawnTick = i;
	}
	if (mIndexId==-1 || mIndexSex==-1 || mIndexAge==-1 ||
		mIndexDrinking==-1 || mIndexFrequencyLevel==-1 || mIndexMonthlyDrinks==-1 ||
		mIndexSpawnTick==-1)
	std::cerr << "Index not found" << std::endl;
}

void Model::doSituationalMechanisms(){
	//Rank 0 checks elapsed duration and throws an error if needed
	if (repast::RepastProcess::instance()->rank() == 0) {
		auto elapsed = std::chrono::duration_cast<std::chrono::minutes>( std::chrono::steady_clock::now() - startTime );
		if (elapsed.count() > 10) {
			std::cerr << "Model. The simulation has run for more than 5 minutes." << std::endl;
			throw MPI::Exception(MPI::ERR_QUOTA);
		}
	}

	repast::SharedContext<Agent>::const_local_iterator iter = context.localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd = context.localEnd();
	while (iter != iterEnd) {
		//Here is where the agents are looped, agent memories can be revised here
		//Both situational mechanisms and the action mechanism are called
		//Agents update in a random order

		(*iter)->doSituation();

		iter++;
	}

	repast::RepastProcess::instance()->synchronizeAgentStates<AgentPackage, AgentPackageProvider, AgentPackageReceiver>(*provider, *receiver);
}


void Model::doActionMechanisms(){
	repast::SharedContext<Agent>::const_local_iterator iter = context.localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd = context.localEnd();
	while (iter != iterEnd) {

		(*iter)->doAction();

		iter++;
	}

	repast::RepastProcess::instance()->synchronizeAgentStates<AgentPackage, AgentPackageProvider, AgentPackageReceiver>(*provider, *receiver);
}


void Model::doTransformationalMechanisms() {
	std::vector<StructuralEntity*>::iterator it = structuralEntityList.begin();
	while(it != structuralEntityList.end()){

		(*it)->doTransformation();

		it++;
	}

	repast::RepastProcess::instance()->synchronizeAgentStates<AgentPackage, AgentPackageProvider, AgentPackageReceiver>(*provider, *receiver);
}

void Model::ageAgents(){
	repast::SharedContext<Agent>::const_local_iterator iter = context.localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd = context.localEnd();
	while (iter != iterEnd) {
		//Age each agent in turn

		(*iter)->ageAgent();

		iter++;
    }

	repast::RepastProcess::instance()->synchronizeAgentStates<AgentPackage, AgentPackageProvider, AgentPackageReceiver>(*provider, *receiver);
}


void Model::killAgents(){
	int agentAgeGroup = 0;
	bool agentSex = FEMALE;
	double agentMortality = 0.0;
	bool agentLives = false;
	std::vector<repast::AgentId> agentKillList;

	repast::SharedContext<Agent>::const_local_iterator iter = context.localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd = context.localEnd();

	while (iter != iterEnd) {
		if ((*iter)->getAge() >= 81) { //kill off agents >= 81 years old
			repast::AgentId id = (*iter)->getId();
			agentKillList.push_back(id);
		} else {
			// Get agent age group and sex.
			agentAgeGroup = (*iter)->findAgeGroup();
			agentSex = (*iter)->getSex();

			// Look up mortality rate
			agentMortality = MORTALITY_RATES[simYear][agentSex][agentAgeGroup];

			// Apply risk
			agentLives = (repast::Random::instance()->nextDouble() > agentMortality/365);

			// Flag this agent to die
			if(agentLives == false && countOfAgents > 1){
				repast::AgentId id = (*iter)->getId();
				agentKillList.push_back(id);
			}
		}
		iter++;
	}


	for (repast::AgentId agentId : agentKillList) {
		//remove local copies of this agent in all other agents. TODO: re-check for multi-core removal.
		removeLocalCopies(agentId);

		// Remove agent
		repast::RepastProcess::instance()->agentRemoved(agentId);
		context.removeAgent(agentId);

		// Update counts + records of agents
		countDiePerYear++;
		countOfAgents--;
	}

	repast::RepastProcess::instance()->synchronizeAgentStates<AgentPackage, AgentPackageProvider, AgentPackageReceiver>(*provider, *receiver);
}


void Model::incrementSimYear(){
	simYear++;
	ageAgents();
}

void Model::spawnAgents(){
	int tempId;
	bool sex;
	int age;
	int drinking;
	int drinkFrequencyLevel;
	int monthlyDrinks;
	double monthlyDrinksSDPct = repast::strToDouble(props->getProperty("temp.monthly.sd.pct"));

	int currentTick = (int)floor(repast::RepastProcess::instance()->getScheduleRunner().currentTick());

	while (!infoTable.empty() && repast::strToInt(infoTable.front()[mIndexSpawnTick]) <= currentTick) {
			std::vector<string> info = infoTable.front();
			infoTable.pop();

			//read the variables from a row of infoTable
			tempId = repast::strToInt(info[mIndexId]);
			sex = repast::strToInt(info[mIndexSex]);
			age = repast::strToInt(info[mIndexAge]);
			drinking = repast::strToInt(info[mIndexDrinking]);
			drinkFrequencyLevel = repast::strToInt(info[mIndexFrequencyLevel]);
			monthlyDrinks = repast::strToInt(info[mIndexMonthlyDrinks]);

			//create an agent using a constructor with variables from file
			int rank = repast::RepastProcess::instance()->rank();
			repast::AgentId id(tempId, rank, 0);
			id.currentRank(rank);
			Agent *agent = new Agent(id, sex, age, drinking,
					drinkFrequencyLevel, monthlyDrinks, monthlyDrinksSDPct);
			context.addAgent(agent);

			//init mediators and theory
			initMediatorAndTheoryFromFile(agent, info);
			connectSpawnAgentToNetwork(agent);

			// Update counts + records of agents
			countSpawnPerYear++;
			countOfAgents++;
	}

	repast::RepastProcess::instance()->synchronizeAgentStates<AgentPackage, AgentPackageProvider, AgentPackageReceiver>(*provider, *receiver);
}

void Model::reset12MonthDrinkers() {
	repast::SharedContext<Agent>::const_local_iterator iter = context.localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd = context.localEnd();
	while (iter != iterEnd) {
		//Age each agent in turn

		(*iter)->reset12MonthDrinker();

		iter++;
	}

	repast::RepastProcess::instance()->synchronizeAgentStates<AgentPackage, AgentPackageProvider, AgentPackageReceiver>(*provider, *receiver);
}

void Model::doDailyActions() {
	int currentTick = (int)floor(repast::RepastProcess::instance()->getScheduleRunner().currentTick());
	if (currentTick % SITUATIONAL_MECHANISM_INTERVAL_TICK == 0) {
		doSituationalMechanisms();
	}
	if (currentTick % ACTION_MECHANISM_INTERVAL_TICK == 0) {
		//situational interval < action interval, so first time action mechanism is triggered,
		// situational mechanism is also calculated once
		if (SITUATIONAL_MECHANISM_INTERVAL_TICK>ACTION_MECHANISM_INTERVAL_TICK &&
				currentTick==ACTION_MECHANISM_INTERVAL_TICK) {
			doSituationalMechanisms();
		}

		doActionMechanisms();
	}
	doTransformationalMechanisms();
	killAgents();
	//spawnAgents(); // Warning: spawning temporarily disabled to have a clean and simple cohort.
}

void Model::doYearlyActions() {
	annualValues->record();
	if (repast::RepastProcess::instance()->getScheduleRunner().currentTick() > 0) {
		incrementSimYear();
		reset12MonthDrinkers();
		doYearlyTheoryActions();
	}
}

void Model::initSchedule(repast::ScheduleRunner& runner){
	runner.scheduleEvent(MECHANISM_START_TICK, MECHANISM_INTERVAL_TICK, \
			repast::Schedule::FunctorPtr(new repast::MethodFunctor<Model> (this, &Model::doDailyActions)));

	runner.scheduleEvent(YEARLY_START_TICK, YEARLY_INTERVAL_TICK, \
						repast::Schedule::FunctorPtr(new repast::MethodFunctor<Model> (this, &Model::doYearlyActions)));

	runner.scheduleStop(stopAt);

	runner.scheduleEndEvent(repast::Schedule::FunctorPtr(new repast::MethodFunctor<repast::DataSet>(annualValues, &repast::DataSet::write)));
}

//states of a character in CSV file
enum class CSVState {
    UnquotedField,
    QuotedField,
    QuotedQuote
};

//read on row in the CSV file
std::vector<std::string> Model::readCSVRow(const std::string &row) {
    CSVState state = CSVState::UnquotedField;
    std::vector<std::string> fields {""};
    size_t i = 0; // index of the current field
    for (char c : row) {
        switch (state) {
            case CSVState::UnquotedField:
                switch (c) {
                    case ',': // end of field
                              fields.push_back(""); i++;
                              break;
                    case '"': state = CSVState::QuotedField;
                              break;
                    default:  fields[i].push_back(c);
                              break; }
                break;
            case CSVState::QuotedField:
                switch (c) {
                    case '"': state = CSVState::QuotedQuote;
                              break;
                    default:  fields[i].push_back(c);
                              break; }
                break;
            case CSVState::QuotedQuote:
                switch (c) {
                    case ',': // , after closing quote
                              fields.push_back(""); i++;
                              state = CSVState::UnquotedField;
                              break;
                    case '"': // "" -> "
                              fields[i].push_back('"');
                              state = CSVState::QuotedField;
                              break;
                    default:  // end of quote
                              state = CSVState::UnquotedField;
                              break; }
                break;
        }
    }
    return fields;
}

// Read CSV file, Excel dialect. Accept "quoted fields ""with quotes"""
std::queue<std::vector<std::string>> Model::readCSV(std::istream &in) {
    std::queue<std::vector<std::string>> table;
    std::string row;
    while (!in.eof()) {
        std::getline(in, row);
        if (in.bad() || in.fail()) {
            break;
        }
        auto fields = readCSVRow(row);
        table.push(fields);
    }
    return table;
}
