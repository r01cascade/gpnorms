#include "DataOut.h"


// List of constructurs and get data methods for the different data columns/variables

OutSumPopulation::OutSumPopulation(repast::SharedContext<Agent>* pPopulation){
	mpPopulation = pPopulation;
}

int OutSumPopulation::getData() {
	return mpPopulation->size();
}

OutSumMale::OutSumMale(repast::SharedContext<Agent>* pPopulation){
	mpPopulation = pPopulation;
}

int OutSumMale::getData() {
	int sum = 0;
	repast::SharedContext<Agent>::const_local_iterator iter =
			mpPopulation->localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd =
			mpPopulation->localEnd();
	while (iter != iterEnd) {
		if ((*iter)->getSex() == MALE) {
			sum += 1;
		}

		iter++;
	}
	return sum;
}

OutSumFemale::OutSumFemale(repast::SharedContext<Agent>* pPopulation){
	mpPopulation = pPopulation;
}

int OutSumFemale::getData() {
	int sum = 0;
	repast::SharedContext<Agent>::const_local_iterator iter =
			mpPopulation->localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd =
			mpPopulation->localEnd();
	while (iter != iterEnd) {
		if ((*iter)->getSex() == FEMALE) {
			sum += 1;
		}

		iter++;
	}
	return sum;
}

OutSumAgeGroup1::OutSumAgeGroup1(repast::SharedContext<Agent>* pPopulation){
	mpPopulation = pPopulation;
}

int OutSumAgeGroup1::getData() {
	int sum = 0;
	repast::SharedContext<Agent>::const_local_iterator iter =
			mpPopulation->localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd =
			mpPopulation->localEnd();
	while (iter != iterEnd) {
		if ((*iter)->getAge() >= 12 && (*iter)->getAge() <= 17) {
			sum += 1;
		}
		iter++;
	}
	return sum;
}

OutSumAgeGroup2::OutSumAgeGroup2(repast::SharedContext<Agent>* pPopulation){
	mpPopulation = pPopulation;
}

int OutSumAgeGroup2::getData() {
	int sum = 0;
	repast::SharedContext<Agent>::const_local_iterator iter =
			mpPopulation->localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd =
			mpPopulation->localEnd();
	while (iter != iterEnd) {
		if ((*iter)->getAge() >= 18 && (*iter)->getAge() <= 34) {
			sum += 1;
		}
		iter++;
	}
	return sum;
}

OutSumAgeGroup3::OutSumAgeGroup3(repast::SharedContext<Agent>* pPopulation){
	mpPopulation = pPopulation;
}

int OutSumAgeGroup3::getData() {
	int sum = 0;
	repast::SharedContext<Agent>::const_local_iterator iter =
			mpPopulation->localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd =
			mpPopulation->localEnd();
	while (iter != iterEnd) {
		if ((*iter)->getAge() >= 35 && (*iter)->getAge() <= 64) {
			sum += 1;
		}
		iter++;
	}
	return sum;
}

OutSumAgeGroup4::OutSumAgeGroup4(repast::SharedContext<Agent>* pPopulation){
	mpPopulation = pPopulation;
}

int OutSumAgeGroup4::getData() {
	int sum = 0;
	repast::SharedContext<Agent>::const_local_iterator iter =
			mpPopulation->localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd =
			mpPopulation->localEnd();
	while (iter != iterEnd) {
		if ((*iter)->getAge() >= 65) {
			sum += 1;
		}
		iter++;
	}
	return sum;
}


OutSum12MonthDrinkers::OutSum12MonthDrinkers(repast::SharedContext<Agent>* pPopulation){
	mpPopulation = pPopulation;
}

int OutSum12MonthDrinkers::getData() {
	int sum = 0;
	repast::SharedContext<Agent>::const_local_iterator iter =
			mpPopulation->localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd =
			mpPopulation->localEnd();
	while (iter != iterEnd) {
		if ((*iter)->isHaveKDrinksOverNDays(365, 1)) {
			sum += 1;
		}
		iter++;
	}
	return sum;
}

OutSum12MonthDrinkersMale::OutSum12MonthDrinkersMale(repast::SharedContext<Agent>* pPopulation){
	mpPopulation = pPopulation;
}

int OutSum12MonthDrinkersMale::getData() {
	int sum = 0;
	repast::SharedContext<Agent>::const_local_iterator iter =
			mpPopulation->localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd =
			mpPopulation->localEnd();
	while (iter != iterEnd) {
		if ((*iter)->isHaveKDrinksOverNDays(365, 1)  && (*iter)->getSex() == MALE) {
			sum += 1;
		}
		iter++;
	}
	return sum;
}


OutSum12MonthDrinkersFemale::OutSum12MonthDrinkersFemale(repast::SharedContext<Agent>* pPopulation){
	mpPopulation = pPopulation;
}

int OutSum12MonthDrinkersFemale::getData() {
	int sum = 0;
	repast::SharedContext<Agent>::const_local_iterator iter =
			mpPopulation->localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd =
			mpPopulation->localEnd();
	while (iter != iterEnd) {
		if ((*iter)->isHaveKDrinksOverNDays(365, 1)  && (*iter)->getSex() == FEMALE) {
			sum += 1;
		}
		iter++;
	}
	return sum;
}

OutSum12MonthDrinkersAgeGroup1::OutSum12MonthDrinkersAgeGroup1(repast::SharedContext<Agent>* pPopulation){
	mpPopulation = pPopulation;
}

int OutSum12MonthDrinkersAgeGroup1::getData() {
	int sum = 0;
	repast::SharedContext<Agent>::const_local_iterator iter =
			mpPopulation->localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd =
			mpPopulation->localEnd();
	while (iter != iterEnd) {
		if ((*iter)->isHaveKDrinksOverNDays(365, 1)  && (*iter)->getAge() >= 12 && (*iter)->getAge() <= 17) {
			sum += 1;
		}
		iter++;
	}
	return sum;
}

OutSum12MonthDrinkersAgeGroup2::OutSum12MonthDrinkersAgeGroup2(repast::SharedContext<Agent>* pPopulation){
	mpPopulation = pPopulation;
}

int OutSum12MonthDrinkersAgeGroup2::getData() {
	int sum = 0;
	repast::SharedContext<Agent>::const_local_iterator iter =
			mpPopulation->localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd =
			mpPopulation->localEnd();
	while (iter != iterEnd) {
		if ((*iter)->isHaveKDrinksOverNDays(365, 1)  && (*iter)->getAge() >= 18 && (*iter)->getAge() <= 34) {
			sum += 1;
		}
		iter++;
	}
	return sum;
}

OutSum12MonthDrinkersAgeGroup3::OutSum12MonthDrinkersAgeGroup3(repast::SharedContext<Agent>* pPopulation){
	mpPopulation = pPopulation;
}

int OutSum12MonthDrinkersAgeGroup3::getData() {
	int sum = 0;
	repast::SharedContext<Agent>::const_local_iterator iter =
			mpPopulation->localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd =
			mpPopulation->localEnd();
	while (iter != iterEnd) {
		if ((*iter)->isHaveKDrinksOverNDays(365, 1)  && (*iter)->getAge() >= 35 && (*iter)->getAge() <= 64) {
			sum += 1;
		}
		iter++;
	}
	return sum;
}

OutSum12MonthDrinkersAgeGroup4::OutSum12MonthDrinkersAgeGroup4(repast::SharedContext<Agent>* pPopulation){
	mpPopulation = pPopulation;
}

int OutSum12MonthDrinkersAgeGroup4::getData() {
	int sum = 0;
	repast::SharedContext<Agent>::const_local_iterator iter =
			mpPopulation->localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd =
			mpPopulation->localEnd();
	while (iter != iterEnd) {
		if ((*iter)->isHaveKDrinksOverNDays(365, 1)  && (*iter)->getAge() >= 65) {
			sum += 1;
		}
		iter++;
	}
	return sum;
}


OutSumQuantMale::OutSumQuantMale(repast::SharedContext<Agent>* pPopulation){
	mpPopulation = pPopulation;
}

double OutSumQuantMale::getData() {
	double sum = 0;
	repast::SharedContext<Agent>::const_local_iterator iter =
			mpPopulation->localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd =
			mpPopulation->localEnd();
	while (iter != iterEnd) {
		if ((*iter)->isHaveKDrinksOverNDays(365, 1) && (*iter)->getSex() == MALE) {
			sum += (*iter)->getAvgDrinksNDays(30, false);
		}
		iter++;
	}
	return sum;
}

OutSumQuantFemale::OutSumQuantFemale(repast::SharedContext<Agent>* pPopulation){
	mpPopulation = pPopulation;
}

double OutSumQuantFemale::getData() {
	double sum = 0;
	repast::SharedContext<Agent>::const_local_iterator iter =
			mpPopulation->localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd =
			mpPopulation->localEnd();
	while (iter != iterEnd) {
		if ((*iter)->isHaveKDrinksOverNDays(365, 1) && (*iter)->getSex() == FEMALE) {
			sum += (*iter)->getAvgDrinksNDays(30,false);
		}
		iter++;
	}
	return sum;
}

OutSumQuantAgeGroup1::OutSumQuantAgeGroup1(repast::SharedContext<Agent>* pPopulation){
	mpPopulation = pPopulation;
}

double OutSumQuantAgeGroup1::getData() {
	double sum = 0;
	repast::SharedContext<Agent>::const_local_iterator iter =
			mpPopulation->localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd =
			mpPopulation->localEnd();
	while (iter != iterEnd) {
		if ((*iter)->isHaveKDrinksOverNDays(365, 1) && (*iter)->getAge() >= 12 && (*iter)->getAge() <= 17) {
			sum += (*iter)->getAvgDrinksNDays(30, false);
		}
		iter++;
	}
	return sum;
}

OutSumQuantAgeGroup2::OutSumQuantAgeGroup2(repast::SharedContext<Agent>* pPopulation){
	mpPopulation = pPopulation;
}

double OutSumQuantAgeGroup2::getData() {
	double sum = 0;
	repast::SharedContext<Agent>::const_local_iterator iter =
			mpPopulation->localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd =
			mpPopulation->localEnd();
	while (iter != iterEnd) {
		if ((*iter)->isHaveKDrinksOverNDays(365, 1) && (*iter)->getAge() >= 18 && (*iter)->getAge() <= 34) {
			sum += (*iter)->getAvgDrinksNDays(30, false);
		}
		iter++;
	}
	return sum;
}

OutSumQuantAgeGroup3::OutSumQuantAgeGroup3(repast::SharedContext<Agent>* pPopulation){
	mpPopulation = pPopulation;
}

double OutSumQuantAgeGroup3::getData() {
	double sum = 0;
	repast::SharedContext<Agent>::const_local_iterator iter =
			mpPopulation->localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd =
			mpPopulation->localEnd();
	while (iter != iterEnd) {
		if ((*iter)->isHaveKDrinksOverNDays(365, 1) && (*iter)->getAge() >= 35 && (*iter)->getAge() <= 64) {
			sum += (*iter)->getAvgDrinksNDays(30, false);
		}
		iter++;
	}
	return sum;
}

OutSumQuantAgeGroup4::OutSumQuantAgeGroup4(repast::SharedContext<Agent>* pPopulation){
	mpPopulation = pPopulation;
}

double OutSumQuantAgeGroup4::getData() {
	double sum = 0;
	repast::SharedContext<Agent>::const_local_iterator iter =
			mpPopulation->localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd =
			mpPopulation->localEnd();
	while (iter != iterEnd) {
		if ((*iter)->isHaveKDrinksOverNDays(365, 1) && (*iter)->getAge() >= 65) {
			sum += (*iter)->getAvgDrinksNDays(30, false);
		}
		iter++;
	}
	return sum;
}


OutSumFreqMale::OutSumFreqMale(repast::SharedContext<Agent>* pPopulation){
	mpPopulation = pPopulation;
}

int OutSumFreqMale::getData() {
	int sum = 0;
	repast::SharedContext<Agent>::const_local_iterator iter =
			mpPopulation->localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd =
			mpPopulation->localEnd();
	while (iter != iterEnd) {
		if ((*iter)->isHaveKDrinksOverNDays(365, 1) && (*iter)->getSex() == MALE) {
			sum += (*iter)->getNumDaysHavingKDrinksOverNDays(30,1);
		}
		iter++;
	}
	return sum;
}

OutSumFreqFemale::OutSumFreqFemale(repast::SharedContext<Agent>* pPopulation){
	mpPopulation = pPopulation;
}

int OutSumFreqFemale::getData() {
	int sum = 0;
	repast::SharedContext<Agent>::const_local_iterator iter =
			mpPopulation->localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd =
			mpPopulation->localEnd();
	while (iter != iterEnd) {
		if ((*iter)->isHaveKDrinksOverNDays(365, 1) && (*iter)->getSex() == FEMALE) {
			sum += (*iter)->getNumDaysHavingKDrinksOverNDays(30,1);
		}
		iter++;
	}
	return sum;
}

OutSumFreqAgeGroup1::OutSumFreqAgeGroup1(repast::SharedContext<Agent>* pPopulation){
	mpPopulation = pPopulation;
}

int OutSumFreqAgeGroup1::getData() {
	int sum = 0;
	repast::SharedContext<Agent>::const_local_iterator iter =
			mpPopulation->localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd =
			mpPopulation->localEnd();
	while (iter != iterEnd) {
		if ((*iter)->isHaveKDrinksOverNDays(365, 1) && (*iter)->getAge() >= 12 && (*iter)->getAge() <= 17) {
			sum += (*iter)->getNumDaysHavingKDrinksOverNDays(30,1);
		}
		iter++;
	}
	return sum;
}

OutSumFreqAgeGroup2::OutSumFreqAgeGroup2(repast::SharedContext<Agent>* pPopulation){
	mpPopulation = pPopulation;
}

int OutSumFreqAgeGroup2::getData() {
	int sum = 0;
	repast::SharedContext<Agent>::const_local_iterator iter =
			mpPopulation->localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd =
			mpPopulation->localEnd();
	while (iter != iterEnd) {
		if ((*iter)->isHaveKDrinksOverNDays(365, 1) && (*iter)->getAge() >= 18 && (*iter)->getAge() <= 34) {
			sum += (*iter)->getNumDaysHavingKDrinksOverNDays(30,1);
		}
		iter++;
	}
	return sum;
}

OutSumFreqAgeGroup3::OutSumFreqAgeGroup3(repast::SharedContext<Agent>* pPopulation){
	mpPopulation = pPopulation;
}

int OutSumFreqAgeGroup3::getData() {
	int sum = 0;
	repast::SharedContext<Agent>::const_local_iterator iter =
			mpPopulation->localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd =
			mpPopulation->localEnd();
	while (iter != iterEnd) {
		if ((*iter)->isHaveKDrinksOverNDays(365, 1) && (*iter)->getAge() >= 35 && (*iter)->getAge() <= 64) {
			sum += (*iter)->getNumDaysHavingKDrinksOverNDays(30,1);
		}
		iter++;
	}
	return sum;
}

OutSumFreqAgeGroup4::OutSumFreqAgeGroup4(repast::SharedContext<Agent>* pPopulation){
	mpPopulation = pPopulation;
}

int OutSumFreqAgeGroup4::getData() {
	int sum = 0;
	repast::SharedContext<Agent>::const_local_iterator iter =
			mpPopulation->localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd =
			mpPopulation->localEnd();
	while (iter != iterEnd) {
		if ((*iter)->isHaveKDrinksOverNDays(365, 1) && (*iter)->getAge() >= 65) {
			sum += (*iter)->getNumDaysHavingKDrinksOverNDays(30,1);
		}
		iter++;
	}
	return sum;
}


OutSumOccasionalHeavyDrinking::OutSumOccasionalHeavyDrinking(repast::SharedContext<Agent>* pPopulation){
	mpPopulation = pPopulation;
}

int OutSumOccasionalHeavyDrinking::getData() {
	int sum = 0;
	repast::SharedContext<Agent>::const_local_iterator iter =
			mpPopulation->localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd =
			mpPopulation->localEnd();
	while (iter != iterEnd) {
		if ((*iter)->isHaveKDrinksOverNDays(365, 1)) {
			sum += (*iter)->getNumDaysHavingKDrinksOverNDays(30,5);
		}
		iter++;
	}
	return sum;
}

/*
// Sum of agents in each age * sex group
OutSumMenAge0::OutSumMenAge0(repast::SharedContext<Agent>* pPopulation){
	mpPopulation = pPopulation;
}

int OutSumMenAge0::getData() {
	int sum = 0;
	repast::SharedContext<Agent>::const_local_iterator iter =
			mpPopulation->localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd =
			mpPopulation->localEnd();
	while (iter != iterEnd) {
		if ((*iter)->getSex() == MALE && (*iter)->findAgeGroup() == 0) {
			sum += 1;
		}
		iter++;
	}
	return sum;
}


OutSumMenAge1::OutSumMenAge1(repast::SharedContext<Agent>* pPopulation){
	mpPopulation = pPopulation;
}

int OutSumMenAge1::getData() {
	int sum = 0;
	repast::SharedContext<Agent>::const_local_iterator iter =
			mpPopulation->localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd =
			mpPopulation->localEnd();
	while (iter != iterEnd) {
		if ((*iter)->getSex() == MALE && (*iter)->findAgeGroup() == 1) {
			sum += 1;
		}
		iter++;
	}
	return sum;
}


OutSumMenAge2::OutSumMenAge2(repast::SharedContext<Agent>* pPopulation){
	mpPopulation = pPopulation;
}

int OutSumMenAge2::getData() {
	int sum = 0;
	repast::SharedContext<Agent>::const_local_iterator iter =
			mpPopulation->localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd =
			mpPopulation->localEnd();
	while (iter != iterEnd) {
		if ((*iter)->getSex() == MALE && (*iter)->findAgeGroup() == 2) {
			sum += 1;
		}
		iter++;
	}
	return sum;
}


OutSumMenAge3::OutSumMenAge3(repast::SharedContext<Agent>* pPopulation){
	mpPopulation = pPopulation;
}

int OutSumMenAge3::getData() {
	int sum = 0;
	repast::SharedContext<Agent>::const_local_iterator iter =
			mpPopulation->localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd =
			mpPopulation->localEnd();
	while (iter != iterEnd) {
		if ((*iter)->getSex() == MALE && (*iter)->findAgeGroup() == 3) {
			sum += 1;
		}
		iter++;
	}
	return sum;
}


OutSumMenAge4::OutSumMenAge4(repast::SharedContext<Agent>* pPopulation){
	mpPopulation = pPopulation;
}

int OutSumMenAge4::getData() {
	int sum = 0;
	repast::SharedContext<Agent>::const_local_iterator iter =
			mpPopulation->localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd =
			mpPopulation->localEnd();
	while (iter != iterEnd) {
		if ((*iter)->getSex() == MALE && (*iter)->findAgeGroup() == 4) {
			sum += 1;
		}
		iter++;
	}
	return sum;
}


OutSumMenAge5::OutSumMenAge5(repast::SharedContext<Agent>* pPopulation){
	mpPopulation = pPopulation;
}

int OutSumMenAge5::getData() {
	int sum = 0;
	repast::SharedContext<Agent>::const_local_iterator iter =
			mpPopulation->localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd =
			mpPopulation->localEnd();
	while (iter != iterEnd) {
		if ((*iter)->getSex() == MALE && (*iter)->findAgeGroup() == 5) {
			sum += 1;
		}
		iter++;
	}
	return sum;
}


OutSumMenAge6::OutSumMenAge6(repast::SharedContext<Agent>* pPopulation){
	mpPopulation = pPopulation;
}

int OutSumMenAge6::getData() {
	int sum = 0;
	repast::SharedContext<Agent>::const_local_iterator iter =
			mpPopulation->localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd =
			mpPopulation->localEnd();
	while (iter != iterEnd) {
		if ((*iter)->getSex() == MALE && (*iter)->findAgeGroup() == 6) {
			sum += 1;
		}
		iter++;
	}
	return sum;
}


OutSumMenAge7::OutSumMenAge7(repast::SharedContext<Agent>* pPopulation){
	mpPopulation = pPopulation;
}

int OutSumMenAge7::getData() {
	int sum = 0;
	repast::SharedContext<Agent>::const_local_iterator iter =
			mpPopulation->localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd =
			mpPopulation->localEnd();
	while (iter != iterEnd) {
		if ((*iter)->getSex() == MALE && (*iter)->findAgeGroup() == 7) {
			sum += 1;
		}
		iter++;
	}
	return sum;
}


OutSumMenAge8::OutSumMenAge8(repast::SharedContext<Agent>* pPopulation){
	mpPopulation = pPopulation;
}

int OutSumMenAge8::getData() {
	int sum = 0;
	repast::SharedContext<Agent>::const_local_iterator iter =
			mpPopulation->localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd =
			mpPopulation->localEnd();
	while (iter != iterEnd) {
		if ((*iter)->getSex() == MALE && (*iter)->findAgeGroup() == 8) {
			sum += 1;
		}
		iter++;
	}
	return sum;
}


OutSumWomenAge0::OutSumWomenAge0(repast::SharedContext<Agent>* pPopulation){
	mpPopulation = pPopulation;
}

int OutSumWomenAge0::getData() {
	int sum = 0;
	repast::SharedContext<Agent>::const_local_iterator iter =
			mpPopulation->localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd =
			mpPopulation->localEnd();
	while (iter != iterEnd) {
		if ((*iter)->getSex() == FEMALE && (*iter)->findAgeGroup() == 0) {
			sum += 1;
		}
		iter++;
	}
	return sum;
}

OutSumWomenAge1::OutSumWomenAge1(repast::SharedContext<Agent>* pPopulation){
	mpPopulation = pPopulation;
}

int OutSumWomenAge1::getData() {
	int sum = 0;
	repast::SharedContext<Agent>::const_local_iterator iter =
			mpPopulation->localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd =
			mpPopulation->localEnd();
	while (iter != iterEnd) {
		if ((*iter)->getSex() == FEMALE && (*iter)->findAgeGroup() == 1) {
			sum += 1;
		}
		iter++;
	}
	return sum;
}


OutSumWomenAge2::OutSumWomenAge2(repast::SharedContext<Agent>* pPopulation){
	mpPopulation = pPopulation;
}

int OutSumWomenAge2::getData() {
	int sum = 0;
	repast::SharedContext<Agent>::const_local_iterator iter =
			mpPopulation->localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd =
			mpPopulation->localEnd();
	while (iter != iterEnd) {
		if ((*iter)->getSex() == FEMALE && (*iter)->findAgeGroup() == 2) {
			sum += 1;
		}
		iter++;
	}
	return sum;
}


OutSumWomenAge3::OutSumWomenAge3(repast::SharedContext<Agent>* pPopulation){
	mpPopulation = pPopulation;
}

int OutSumWomenAge3::getData() {
	int sum = 0;
	repast::SharedContext<Agent>::const_local_iterator iter =
			mpPopulation->localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd =
			mpPopulation->localEnd();
	while (iter != iterEnd) {
		if ((*iter)->getSex() == FEMALE && (*iter)->findAgeGroup() == 3) {
			sum += 1;
		}
		iter++;
	}
	return sum;
}


OutSumWomenAge4::OutSumWomenAge4(repast::SharedContext<Agent>* pPopulation){
	mpPopulation = pPopulation;
}

int OutSumWomenAge4::getData() {
	int sum = 0;
	repast::SharedContext<Agent>::const_local_iterator iter =
			mpPopulation->localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd =
			mpPopulation->localEnd();
	while (iter != iterEnd) {
		if ((*iter)->getSex() == FEMALE && (*iter)->findAgeGroup() == 4) {
			sum += 1;
		}
		iter++;
	}
	return sum;
}


OutSumWomenAge5::OutSumWomenAge5(repast::SharedContext<Agent>* pPopulation){
	mpPopulation = pPopulation;
}

int OutSumWomenAge5::getData() {
	int sum = 0;
	repast::SharedContext<Agent>::const_local_iterator iter =
			mpPopulation->localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd =
			mpPopulation->localEnd();
	while (iter != iterEnd) {
		if ((*iter)->getSex() == FEMALE && (*iter)->findAgeGroup() == 5) {
			sum += 1;
		}
		iter++;
	}
	return sum;
}


OutSumWomenAge6::OutSumWomenAge6(repast::SharedContext<Agent>* pPopulation){
	mpPopulation = pPopulation;
}

int OutSumWomenAge6::getData() {
	int sum = 0;
	repast::SharedContext<Agent>::const_local_iterator iter =
			mpPopulation->localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd =
			mpPopulation->localEnd();
	while (iter != iterEnd) {
		if ((*iter)->getSex() == FEMALE && (*iter)->findAgeGroup() == 6) {
			sum += 1;
		}
		iter++;
	}
	return sum;
}


OutSumWomenAge7::OutSumWomenAge7(repast::SharedContext<Agent>* pPopulation){
	mpPopulation = pPopulation;
}

int OutSumWomenAge7::getData() {
	int sum = 0;
	repast::SharedContext<Agent>::const_local_iterator iter =
			mpPopulation->localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd =
			mpPopulation->localEnd();
	while (iter != iterEnd) {
		if ((*iter)->getSex() == FEMALE && (*iter)->findAgeGroup() == 7) {
			sum += 1;
		}
		iter++;
	}
	return sum;
}


OutSumWomenAge8::OutSumWomenAge8(repast::SharedContext<Agent>* pPopulation){
	mpPopulation = pPopulation;
}

int OutSumWomenAge8::getData() {
	int sum = 0;
	repast::SharedContext<Agent>::const_local_iterator iter =
			mpPopulation->localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd =
			mpPopulation->localEnd();
	while (iter != iterEnd) {
		if ((*iter)->getSex() == FEMALE && (*iter)->findAgeGroup() == 8) {
			sum += 1;
		}
		iter++;
	}
	return sum;
}


// Data output for prevalence
// Total of drinkers for each age * sex group as per scheduler

OutTotalDrinkersMenAge0::OutTotalDrinkersMenAge0(repast::SharedContext<Agent>* pPopulation){
	mpPopulation = pPopulation;
}

int OutTotalDrinkersMenAge0::getData() {
	int sum = 0;
	repast::SharedContext<Agent>::const_local_iterator iter =
			mpPopulation->localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd =
			mpPopulation->localEnd();
	while (iter != iterEnd) {
		if ((*iter)->getSex() == MALE && (*iter)->findAgeGroup() == 0) {
			sum += (*iter)->isHaveKDrinksOverNDays(30, 1);
		}
		iter++;
	}
	return sum;
}


OutTotalDrinkersMenAge1::OutTotalDrinkersMenAge1(repast::SharedContext<Agent>* pPopulation){
	mpPopulation = pPopulation;
}

int OutTotalDrinkersMenAge1::getData() {
	int sum = 0;
	repast::SharedContext<Agent>::const_local_iterator iter =
			mpPopulation->localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd =
			mpPopulation->localEnd();
	while (iter != iterEnd) {
		if ((*iter)->getSex() == MALE && (*iter)->findAgeGroup() == 1) {
			sum += (*iter)->isHaveKDrinksOverNDays(30, 1);
		}
		iter++;
	}
	return sum;
}


OutTotalDrinkersMenAge2::OutTotalDrinkersMenAge2(repast::SharedContext<Agent>* pPopulation){
	mpPopulation = pPopulation;
}

int OutTotalDrinkersMenAge2::getData() {
	int sum = 0;
	repast::SharedContext<Agent>::const_local_iterator iter =
			mpPopulation->localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd =
			mpPopulation->localEnd();
	while (iter != iterEnd) {
		if ((*iter)->getSex() == MALE && (*iter)->findAgeGroup() == 2) {
			sum += (*iter)->isHaveKDrinksOverNDays(30, 1);
		}
		iter++;
	}
	return sum;
}


OutTotalDrinkersMenAge3::OutTotalDrinkersMenAge3(repast::SharedContext<Agent>* pPopulation){
	mpPopulation = pPopulation;
}

int OutTotalDrinkersMenAge3::getData() {
	int sum = 0;
	repast::SharedContext<Agent>::const_local_iterator iter =
			mpPopulation->localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd =
			mpPopulation->localEnd();
	while (iter != iterEnd) {
		if ((*iter)->getSex() == MALE && (*iter)->findAgeGroup() == 3) {
			sum += (*iter)->isHaveKDrinksOverNDays(30, 1);
		}
		iter++;
	}
	return sum;
}


OutTotalDrinkersMenAge4::OutTotalDrinkersMenAge4(repast::SharedContext<Agent>* pPopulation){
	mpPopulation = pPopulation;
}

int OutTotalDrinkersMenAge4::getData() {
	int sum = 0;
	repast::SharedContext<Agent>::const_local_iterator iter =
			mpPopulation->localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd =
			mpPopulation->localEnd();
	while (iter != iterEnd) {
		if ((*iter)->getSex() == MALE && (*iter)->findAgeGroup() == 4) {
			sum += (*iter)->isHaveKDrinksOverNDays(30, 1);
		}
		iter++;
	}
	return sum;
}


OutTotalDrinkersMenAge5::OutTotalDrinkersMenAge5(repast::SharedContext<Agent>* pPopulation){
	mpPopulation = pPopulation;
}

int OutTotalDrinkersMenAge5::getData() {
	int sum = 0;
	repast::SharedContext<Agent>::const_local_iterator iter =
			mpPopulation->localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd =
			mpPopulation->localEnd();
	while (iter != iterEnd) {
		if ((*iter)->getSex() == MALE && (*iter)->findAgeGroup() == 5) {
			sum += (*iter)->isHaveKDrinksOverNDays(30, 1);
		}
		iter++;
	}
	return sum;
}


OutTotalDrinkersMenAge6::OutTotalDrinkersMenAge6(repast::SharedContext<Agent>* pPopulation){
	mpPopulation = pPopulation;
}

int OutTotalDrinkersMenAge6::getData() {
	int sum = 0;
	repast::SharedContext<Agent>::const_local_iterator iter =
			mpPopulation->localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd =
			mpPopulation->localEnd();
	while (iter != iterEnd) {
		if ((*iter)->getSex() == MALE && (*iter)->findAgeGroup() == 6) {
			sum += (*iter)->isHaveKDrinksOverNDays(30, 1);
		}
		iter++;
	}
	return sum;
}


OutTotalDrinkersMenAge7::OutTotalDrinkersMenAge7(repast::SharedContext<Agent>* pPopulation){
	mpPopulation = pPopulation;
}

int OutTotalDrinkersMenAge7::getData() {
	int sum = 0;
	repast::SharedContext<Agent>::const_local_iterator iter =
			mpPopulation->localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd =
			mpPopulation->localEnd();
	while (iter != iterEnd) {
		if ((*iter)->getSex() == MALE && (*iter)->findAgeGroup() == 7) {
			sum += (*iter)->isHaveKDrinksOverNDays(30, 1);
		}
		iter++;
	}
	return sum;
}


OutTotalDrinkersMenAge8::OutTotalDrinkersMenAge8(repast::SharedContext<Agent>* pPopulation){
	mpPopulation = pPopulation;
}

int OutTotalDrinkersMenAge8::getData() {
	int sum = 0;
	repast::SharedContext<Agent>::const_local_iterator iter =
			mpPopulation->localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd =
			mpPopulation->localEnd();
	while (iter != iterEnd) {
		if ((*iter)->getSex() == MALE && (*iter)->findAgeGroup() == 8) {
			sum += (*iter)->isHaveKDrinksOverNDays(30, 1);
		}
		iter++;
	}
	return sum;
}


OutTotalDrinkersWomenAge0::OutTotalDrinkersWomenAge0(repast::SharedContext<Agent>* pPopulation){
	mpPopulation = pPopulation;
}

int OutTotalDrinkersWomenAge0::getData() {
	int sum = 0;
	repast::SharedContext<Agent>::const_local_iterator iter =
			mpPopulation->localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd =
			mpPopulation->localEnd();
	while (iter != iterEnd) {
		if ((*iter)->getSex() == FEMALE && (*iter)->findAgeGroup() == 0) {
			sum += (*iter)->isHaveKDrinksOverNDays(30, 1);
		}
		iter++;
	}
	return sum;
}


OutTotalDrinkersWomenAge1::OutTotalDrinkersWomenAge1(repast::SharedContext<Agent>* pPopulation){
	mpPopulation = pPopulation;
}

int OutTotalDrinkersWomenAge1::getData() {
	int sum = 0;
	repast::SharedContext<Agent>::const_local_iterator iter =
			mpPopulation->localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd =
			mpPopulation->localEnd();
	while (iter != iterEnd) {
		if ((*iter)->getSex() == FEMALE && (*iter)->findAgeGroup() == 1) {
			sum += (*iter)->isHaveKDrinksOverNDays(30, 1);
		}
		iter++;
	}
	return sum;
}


OutTotalDrinkersWomenAge2::OutTotalDrinkersWomenAge2(repast::SharedContext<Agent>* pPopulation){
	mpPopulation = pPopulation;
}

int OutTotalDrinkersWomenAge2::getData() {
	int sum = 0;
	repast::SharedContext<Agent>::const_local_iterator iter =
			mpPopulation->localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd =
			mpPopulation->localEnd();
	while (iter != iterEnd) {
		if ((*iter)->getSex() == FEMALE && (*iter)->findAgeGroup() == 2) {
			sum += (*iter)->isHaveKDrinksOverNDays(30, 1);
		}
		iter++;
	}
	return sum;
}


OutTotalDrinkersWomenAge3::OutTotalDrinkersWomenAge3(repast::SharedContext<Agent>* pPopulation){
	mpPopulation = pPopulation;
}

int OutTotalDrinkersWomenAge3::getData() {
	int sum = 0;
	repast::SharedContext<Agent>::const_local_iterator iter =
			mpPopulation->localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd =
			mpPopulation->localEnd();
	while (iter != iterEnd) {
		if ((*iter)->getSex() == FEMALE && (*iter)->findAgeGroup() == 3) {
			sum += (*iter)->isHaveKDrinksOverNDays(30, 1);
		}
		iter++;
	}
	return sum;
}


OutTotalDrinkersWomenAge4::OutTotalDrinkersWomenAge4(repast::SharedContext<Agent>* pPopulation){
	mpPopulation = pPopulation;
}

int OutTotalDrinkersWomenAge4::getData() {
	int sum = 0;
	repast::SharedContext<Agent>::const_local_iterator iter =
			mpPopulation->localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd =
			mpPopulation->localEnd();
	while (iter != iterEnd) {
		if ((*iter)->getSex() == FEMALE && (*iter)->findAgeGroup() == 4) {
			sum += (*iter)->isHaveKDrinksOverNDays(30, 1);
		}
		iter++;
	}
	return sum;
}


OutTotalDrinkersWomenAge5::OutTotalDrinkersWomenAge5(repast::SharedContext<Agent>* pPopulation){
	mpPopulation = pPopulation;
}

int OutTotalDrinkersWomenAge5::getData() {
	int sum = 0;
	repast::SharedContext<Agent>::const_local_iterator iter =
			mpPopulation->localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd =
			mpPopulation->localEnd();
	while (iter != iterEnd) {
		if ((*iter)->getSex() == FEMALE && (*iter)->findAgeGroup() == 5) {
			sum += (*iter)->isHaveKDrinksOverNDays(30, 1);
		}
		iter++;
	}
	return sum;
}


OutTotalDrinkersWomenAge6::OutTotalDrinkersWomenAge6(repast::SharedContext<Agent>* pPopulation){
	mpPopulation = pPopulation;
}

int OutTotalDrinkersWomenAge6::getData() {
	int sum = 0;
	repast::SharedContext<Agent>::const_local_iterator iter =
			mpPopulation->localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd =
			mpPopulation->localEnd();
	while (iter != iterEnd) {
		if ((*iter)->getSex() == FEMALE && (*iter)->findAgeGroup() == 6) {
			sum += (*iter)->isHaveKDrinksOverNDays(30, 1);
		}
		iter++;
	}
	return sum;
}


OutTotalDrinkersWomenAge7::OutTotalDrinkersWomenAge7(repast::SharedContext<Agent>* pPopulation){
	mpPopulation = pPopulation;
}

int OutTotalDrinkersWomenAge7::getData() {
	int sum = 0;
	repast::SharedContext<Agent>::const_local_iterator iter =
			mpPopulation->localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd =
			mpPopulation->localEnd();
	while (iter != iterEnd) {
		if ((*iter)->getSex() == FEMALE && (*iter)->findAgeGroup() == 7) {
			sum += (*iter)->isHaveKDrinksOverNDays(30, 1);
		}
		iter++;
	}
	return sum;
}


OutTotalDrinkersWomenAge8::OutTotalDrinkersWomenAge8(repast::SharedContext<Agent>* pPopulation){
	mpPopulation = pPopulation;
}

int OutTotalDrinkersWomenAge8::getData() {
	int sum = 0;
	repast::SharedContext<Agent>::const_local_iterator iter =
			mpPopulation->localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd =
			mpPopulation->localEnd();
	while (iter != iterEnd) {
		if ((*iter)->getSex() == FEMALE && (*iter)->findAgeGroup() == 8) {
			sum += (*iter)->isHaveKDrinksOverNDays(30, 1);
		}
		iter++;
	}
	return sum;
}

// Data output for quantities
// Sum of drinks for each age * sex group as per scheduler
OutSumDrinkMenAge0::OutSumDrinkMenAge0(repast::SharedContext<Agent>* pPopulation){
	mpPopulation = pPopulation;
}

int OutSumDrinkMenAge0::getData() {
	int sum = 0;
	repast::SharedContext<Agent>::const_local_iterator iter =
			mpPopulation->localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd =
			mpPopulation->localEnd();
	while (iter != iterEnd) {
		if ((*iter)->getSex() == MALE && (*iter)->findAgeGroup() == 0 && (*iter)->isHaveKDrinksOverNDays(30, 1) == true) {
			sum += (*iter)->getAvgDrinksNDays(30);
		}
		iter++;
	}
	return sum;
}


OutSumDrinkMenAge1::OutSumDrinkMenAge1(repast::SharedContext<Agent>* pPopulation){
	mpPopulation = pPopulation;
}

int OutSumDrinkMenAge1::getData() {
	int sum = 0;
	repast::SharedContext<Agent>::const_local_iterator iter =
			mpPopulation->localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd =
			mpPopulation->localEnd();
	while (iter != iterEnd) {
		if ((*iter)->getSex() == MALE && (*iter)->findAgeGroup() == 1 && (*iter)->isHaveKDrinksOverNDays(30, 1) == true) {
			sum += (*iter)->getAvgDrinksNDays(30);
		}
		iter++;
	}
	return sum;
}


OutSumDrinkMenAge2::OutSumDrinkMenAge2(repast::SharedContext<Agent>* pPopulation){
	mpPopulation = pPopulation;
}

int OutSumDrinkMenAge2::getData() {
	int sum = 0;
	repast::SharedContext<Agent>::const_local_iterator iter =
			mpPopulation->localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd =
			mpPopulation->localEnd();
	while (iter != iterEnd) {
		if ((*iter)->getSex() == MALE && (*iter)->findAgeGroup() == 2 && (*iter)->isHaveKDrinksOverNDays(30, 1) == true) {
			sum += (*iter)->getAvgDrinksNDays(30);
		}
		iter++;
	}
	return sum;
}


OutSumDrinkMenAge3::OutSumDrinkMenAge3(repast::SharedContext<Agent>* pPopulation){
	mpPopulation = pPopulation;
}

int OutSumDrinkMenAge3::getData() {
	int sum = 0;
	repast::SharedContext<Agent>::const_local_iterator iter =
			mpPopulation->localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd =
			mpPopulation->localEnd();
	while (iter != iterEnd) {
		if ((*iter)->getSex() == MALE && (*iter)->findAgeGroup() == 3 && (*iter)->isHaveKDrinksOverNDays(30, 1) == true) {
			sum += (*iter)->getAvgDrinksNDays(30);
		}
		iter++;
	}
	return sum;
}


OutSumDrinkMenAge4::OutSumDrinkMenAge4(repast::SharedContext<Agent>* pPopulation){
	mpPopulation = pPopulation;
}

int OutSumDrinkMenAge4::getData() {
	int sum = 0;
	repast::SharedContext<Agent>::const_local_iterator iter =
			mpPopulation->localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd =
			mpPopulation->localEnd();
	while (iter != iterEnd) {
		if ((*iter)->getSex() == MALE && (*iter)->findAgeGroup() == 4 && (*iter)->isHaveKDrinksOverNDays(30, 1) == true) {
			sum += (*iter)->getAvgDrinksNDays(30);
		}
		iter++;
	}
	return sum;
}


OutSumDrinkMenAge5::OutSumDrinkMenAge5(repast::SharedContext<Agent>* pPopulation){
	mpPopulation = pPopulation;
}

int OutSumDrinkMenAge5::getData() {
	int sum = 0;
	repast::SharedContext<Agent>::const_local_iterator iter =
			mpPopulation->localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd =
			mpPopulation->localEnd();
	while (iter != iterEnd) {
		if ((*iter)->getSex() == MALE && (*iter)->findAgeGroup() == 5 && (*iter)->isHaveKDrinksOverNDays(30, 1) == true) {
			sum += (*iter)->getAvgDrinksNDays(30);
		}
		iter++;
	}
	return sum;
}


OutSumDrinkMenAge6::OutSumDrinkMenAge6(repast::SharedContext<Agent>* pPopulation){
	mpPopulation = pPopulation;
}

int OutSumDrinkMenAge6::getData() {
	int sum = 0;
	repast::SharedContext<Agent>::const_local_iterator iter =
			mpPopulation->localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd =
			mpPopulation->localEnd();
	while (iter != iterEnd) {
		if ((*iter)->getSex() == MALE && (*iter)->findAgeGroup() == 6 && (*iter)->isHaveKDrinksOverNDays(30, 1) == true) {
			sum += (*iter)->getAvgDrinksNDays(30);
		}
		iter++;
	}
	return sum;
}


OutSumDrinkMenAge7::OutSumDrinkMenAge7(repast::SharedContext<Agent>* pPopulation){
	mpPopulation = pPopulation;
}

int OutSumDrinkMenAge7::getData() {
	int sum = 0;
	repast::SharedContext<Agent>::const_local_iterator iter =
			mpPopulation->localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd =
			mpPopulation->localEnd();
	while (iter != iterEnd) {
		if ((*iter)->getSex() == MALE && (*iter)->findAgeGroup() == 7 && (*iter)->isHaveKDrinksOverNDays(30, 1) == true) {
			sum += (*iter)->getAvgDrinksNDays(30);
		}
		iter++;
	}
	return sum;
}


OutSumDrinkMenAge8::OutSumDrinkMenAge8(repast::SharedContext<Agent>* pPopulation){
	mpPopulation = pPopulation;
}

int OutSumDrinkMenAge8::getData() {
	int sum = 0;
	repast::SharedContext<Agent>::const_local_iterator iter =
			mpPopulation->localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd =
			mpPopulation->localEnd();
	while (iter != iterEnd) {
		if ((*iter)->getSex() == MALE && (*iter)->findAgeGroup() == 8 && (*iter)->isHaveKDrinksOverNDays(30, 1) == true) {
			sum += (*iter)->getAvgDrinksNDays(30);
		}
		iter++;
	}
	return sum;
}


OutSumDrinkWomenAge0::OutSumDrinkWomenAge0(repast::SharedContext<Agent>* pPopulation){
	mpPopulation = pPopulation;
}

int OutSumDrinkWomenAge0::getData() {
	int sum = 0;
	repast::SharedContext<Agent>::const_local_iterator iter =
			mpPopulation->localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd =
			mpPopulation->localEnd();
	while (iter != iterEnd) {
		if ((*iter)->getSex() == FEMALE && (*iter)->findAgeGroup() == 0 && (*iter)->isHaveKDrinksOverNDays(30, 1) == true) {
			sum += (*iter)->getAvgDrinksNDays(30);
		}
		iter++;
	}
	return sum;
}


OutSumDrinkWomenAge1::OutSumDrinkWomenAge1(repast::SharedContext<Agent>* pPopulation){
	mpPopulation = pPopulation;
}

int OutSumDrinkWomenAge1::getData() {
	int sum = 0;
	repast::SharedContext<Agent>::const_local_iterator iter =
			mpPopulation->localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd =
			mpPopulation->localEnd();
	while (iter != iterEnd) {
		if ((*iter)->getSex() == FEMALE && (*iter)->findAgeGroup() == 1 && (*iter)->isHaveKDrinksOverNDays(30, 1) == true) {
			sum += (*iter)->getAvgDrinksNDays(30);
		}
		iter++;
	}
	return sum;
}


OutSumDrinkWomenAge2::OutSumDrinkWomenAge2(repast::SharedContext<Agent>* pPopulation){
	mpPopulation = pPopulation;
}

int OutSumDrinkWomenAge2::getData() {
	int sum = 0;
	repast::SharedContext<Agent>::const_local_iterator iter =
			mpPopulation->localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd =
			mpPopulation->localEnd();
	while (iter != iterEnd) {
		if ((*iter)->getSex() == FEMALE && (*iter)->findAgeGroup() == 2 && (*iter)->isHaveKDrinksOverNDays(30, 1) == true) {
			sum += (*iter)->getAvgDrinksNDays(30);
		}
		iter++;
	}
	return sum;
}


OutSumDrinkWomenAge3::OutSumDrinkWomenAge3(repast::SharedContext<Agent>* pPopulation){
	mpPopulation = pPopulation;
}

int OutSumDrinkWomenAge3::getData() {
	int sum = 0;
	repast::SharedContext<Agent>::const_local_iterator iter =
			mpPopulation->localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd =
			mpPopulation->localEnd();
	while (iter != iterEnd) {
		if ((*iter)->getSex() == FEMALE && (*iter)->findAgeGroup() == 3 && (*iter)->isHaveKDrinksOverNDays(30, 1) == true) {
			sum += (*iter)->getAvgDrinksNDays(30);
		}
		iter++;
	}
	return sum;
}


OutSumDrinkWomenAge4::OutSumDrinkWomenAge4(repast::SharedContext<Agent>* pPopulation){
	mpPopulation = pPopulation;
}

int OutSumDrinkWomenAge4::getData() {
	int sum = 0;
	repast::SharedContext<Agent>::const_local_iterator iter =
			mpPopulation->localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd =
			mpPopulation->localEnd();
	while (iter != iterEnd) {
		if ((*iter)->getSex() == FEMALE && (*iter)->findAgeGroup() == 4 && (*iter)->isHaveKDrinksOverNDays(30, 1) == true) {
			sum += (*iter)->getAvgDrinksNDays(30);
		}
		iter++;
	}
	return sum;
}


OutSumDrinkWomenAge5::OutSumDrinkWomenAge5(repast::SharedContext<Agent>* pPopulation){
	mpPopulation = pPopulation;
}

int OutSumDrinkWomenAge5::getData() {
	int sum = 0;
	repast::SharedContext<Agent>::const_local_iterator iter =
			mpPopulation->localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd =
			mpPopulation->localEnd();
	while (iter != iterEnd) {
		if ((*iter)->getSex() == FEMALE && (*iter)->findAgeGroup() == 5 && (*iter)->isHaveKDrinksOverNDays(30, 1) == true) {
			sum += (*iter)->getAvgDrinksNDays(30);
		}
		iter++;
	}
	return sum;
}


OutSumDrinkWomenAge6::OutSumDrinkWomenAge6(repast::SharedContext<Agent>* pPopulation){
	mpPopulation = pPopulation;
}

int OutSumDrinkWomenAge6::getData() {
	int sum = 0;
	repast::SharedContext<Agent>::const_local_iterator iter =
			mpPopulation->localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd =
			mpPopulation->localEnd();
	while (iter != iterEnd) {
		if ((*iter)->getSex() == FEMALE && (*iter)->findAgeGroup() == 6 && (*iter)->isHaveKDrinksOverNDays(30, 1) == true) {
			sum += (*iter)->getAvgDrinksNDays(30);
		}
		iter++;
	}
	return sum;
}


OutSumDrinkWomenAge7::OutSumDrinkWomenAge7(repast::SharedContext<Agent>* pPopulation){
	mpPopulation = pPopulation;
}

int OutSumDrinkWomenAge7::getData() {
	int sum = 0;
	repast::SharedContext<Agent>::const_local_iterator iter =
			mpPopulation->localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd =
			mpPopulation->localEnd();
	while (iter != iterEnd) {
		if ((*iter)->getSex() == FEMALE && (*iter)->findAgeGroup() == 7  && (*iter)->isHaveKDrinksOverNDays(30, 1) == true) {
			sum += (*iter)->getAvgDrinksNDays(30);
		}
		iter++;
	}
	return sum;
}


OutSumDrinkWomenAge8::OutSumDrinkWomenAge8(repast::SharedContext<Agent>* pPopulation){
	mpPopulation = pPopulation;
}

int OutSumDrinkWomenAge8::getData() {
	int sum = 0;
	repast::SharedContext<Agent>::const_local_iterator iter =
			mpPopulation->localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd =
			mpPopulation->localEnd();
	while (iter != iterEnd) {
		if ((*iter)->getSex() == FEMALE && (*iter)->findAgeGroup() == 8 && (*iter)->isHaveKDrinksOverNDays(30, 1) == true) {
			sum += (*iter)->getAvgDrinksNDays(30);
		}
		iter++;
	}
	return sum;
}
*/

