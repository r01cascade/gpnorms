#include "DescriptiveNormEntity.h"
#include "vector"
#include "globals.h"
#include "repast_hpc/Utilities.h"
#include "repast_hpc/RepastProcess.h"
#include <math.h>
#include <stdio.h>

DescriptiveNormEntity::DescriptiveNormEntity(
		std::vector<Regulator*> regulatorList, std::vector<double> powerList, int transformationalInterval,
		repast::SharedContext<Agent> *context) :
			StructuralEntity(regulatorList, powerList, transformationalInterval) {
	mpContext = context;

	//create a 2D arrays: NUM_SEX x NUM_AGE_GROUPS
	mpAvgIsDrinking = new double*[NUM_SEX];
	mpAvgNumberDrinks = new double*[NUM_SEX];
	mpSdNumberDrinks = new double*[NUM_SEX];
	for(int i = 0; i < NUM_SEX; ++i) {
		mpAvgIsDrinking[i] = new double[NUM_AGE_GROUPS];
		mpAvgNumberDrinks[i] = new double[NUM_AGE_GROUPS];
		mpSdNumberDrinks[i] = new double[NUM_AGE_GROUPS];
	}

	//init arrays from global values
	for (int i=0; i<NUM_SEX; ++i) {
		for (int j=0; j<NUM_AGE_GROUPS; ++j) {
			mpAvgIsDrinking[i][j] = 0;
			mpAvgNumberDrinks[i][j] = 0;
			mpSdNumberDrinks[i][j] = 0;
		}
	}
}

void DescriptiveNormEntity::updateDescriptiveGroupDrinking() {
	//reset avg arrays
	for (int i=0; i<NUM_SEX; ++i) {
		for (int j=0; j<NUM_AGE_GROUPS; ++j) {
			mpAvgIsDrinking[i][j] = 0;
			mpAvgNumberDrinks[i][j] = 0;
			mpSdNumberDrinks[i][j] = 0;
		}
	}

	//create arrays to count agents in a group //TODO: consider using vector 2d
	int** pCountInGroup = new int*[NUM_SEX];
	int** pCountDrinkersInGroup = new int*[NUM_SEX];
	for(int i = 0; i < NUM_SEX; ++i) {
		pCountInGroup[i] = new int[NUM_AGE_GROUPS];
		pCountDrinkersInGroup[i] = new int[NUM_AGE_GROUPS];
	}
	for(int i = 0; i < NUM_SEX; ++i) {
		for (int j=0; j<NUM_AGE_GROUPS; ++j) {
			pCountInGroup[i][j] = 0;
			pCountDrinkersInGroup[i][j] = 0;
		}
	}

	//loop through agents to calculate average
	std::vector<Agent*> agents;
	mpContext->selectAgents(mpContext->size(), agents);
	std::vector<Agent*>::const_iterator iter = agents.begin();
	std::vector<Agent*>::const_iterator iterEnd = agents.end();
	while (iter != iterEnd) {
		int sex = (*iter)->getSex();
		int ageGroup = (*iter)->findAgeGroup();

		//mpAvgIsDrinking[sex][ageGroup] += (*iter)->isDrinkingToday();
		mpAvgIsDrinking[sex][ageGroup] += ((double)(*iter)->getNumDaysHavingKDrinksOverNDays(N_DAYS_DESCRIPTIVE, 1)) / (double)N_DAYS_DESCRIPTIVE;
		pCountInGroup[sex][ageGroup]++;

		if ((*iter)->isHaveKDrinksOverNDays(N_DAYS_DESCRIPTIVE,1)) {
			pCountDrinkersInGroup[sex][ageGroup]++;
			mpAvgNumberDrinks[sex][ageGroup] += (*iter)->getAvgDrinksNDays(N_DAYS_DESCRIPTIVE, true);
			//std::cout << "sum of average drinks per occasion " << mpAvgNumberDrinks[sex][ageGroup] << std::endl;
		}

		iter++;
	}

	//loop again to calculate standard deviation
	iter = agents.begin();
	iterEnd = agents.end();
	while (iter != iterEnd) {
		int sex = (*iter)->getSex();
		int ageGroup = (*iter)->findAgeGroup();

		if ((*iter)->isHaveKDrinksOverNDays(N_DAYS_DESCRIPTIVE ,1)) {
			//pCountInGroup[sex][ageGroup]++;
			mpSdNumberDrinks[sex][ageGroup] += pow( (*iter)->getAvgDrinksNDays(N_DAYS_DESCRIPTIVE, true) - mpAvgNumberDrinks[sex][ageGroup] ,2);
			//printf("%.2f\t%.2f\t%.2f\n",(*iter)->getAvgDrinksNDays(N_DAYS_DESCRIPTIVE),mpAvgNumberDrinks[sex][ageGroup],mpSdNumberDrinks[sex][ageGroup]);
		} else {
			//printf("%.s\t%.s\t%.2f\n","_","_",mpSdNumberDrinks[sex][ageGroup]);
		}

		iter++;
	}

	//caculate aggregate info
	for (int i=0; i<NUM_SEX; ++i)
		for (int j=0; j<NUM_AGE_GROUPS; ++j) {
			if (pCountInGroup[i][j] == 0)
				mpAvgIsDrinking[i][j] = 0;
			else mpAvgIsDrinking[i][j] /= pCountInGroup[i][j];

			if (pCountDrinkersInGroup[i][j] == 0) {
				mpAvgNumberDrinks[i][j] = 0;
				mpSdNumberDrinks[i][j] = 0;
			} else {
				//double tempBefore = mpSdNumberDrinks[i][j];
				mpAvgNumberDrinks[i][j] /= pCountDrinkersInGroup[i][j];
				mpSdNumberDrinks[i][j] = sqrt(mpSdNumberDrinks[i][j] / pCountDrinkersInGroup[i][j]);
				//printf("=== %d\t%f\t%f\n",pCountDrinkersInGroup[i][j],tempBefore,mpSdNumberDrinks[i][j]);
			}
		}

	//delete count array
	for(int i = 0; i < NUM_SEX; ++i) {
		delete[] pCountInGroup[i];
		delete[] pCountDrinkersInGroup[i];
	}
	delete[] pCountInGroup;
	delete[] pCountDrinkersInGroup;
}

double DescriptiveNormEntity::getAvgIsDrinking(int sex, int ageGroup) {
	return mpAvgIsDrinking[sex][ageGroup];
}

double DescriptiveNormEntity::getAvgNumberDrinks(int sex, int ageGroup) {
	return mpAvgNumberDrinks[sex][ageGroup];
}

double DescriptiveNormEntity::getSdNumberDrinks(int sex, int ageGroup) {
	return mpSdNumberDrinks[sex][ageGroup];
}

void DescriptiveNormEntity::doTransformation() {
	int currentTick = (int)floor(repast::RepastProcess::instance()->getScheduleRunner().currentTick());
	if (currentTick % mTransformationalInterval == 0) {
		mTransformationalTriggerCount++;

		updateDescriptiveGroupDrinking(); //update avg drinking values
	}
}
