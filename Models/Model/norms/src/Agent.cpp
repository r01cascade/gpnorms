/* Agent.cpp */

#include "Agent.h"
#include "globals.h"

Agent::Agent(repast::AgentId id){
	// These are temporary parameterizations that will be replaced by the calibration process
	mId = id;
	mAge = int (repast::Random::instance()->nextDouble() * 100);
    mSex = bool (repast::Random::instance()->nextDouble() < 0.5 ? 1 : 0);
    mIsDrinkingToday = bool (repast::Random::instance()->nextDouble() < 0.5 ? 1 : 0);
    mNumberDrinksToday = 0;
    mIs12MonthDrinker = false;
    mDrinkFrequencyLevel = 1 + repast::Random::instance()->nextDouble() * MAX_DRINK_LEVEL;

	initDispositions();
}

Agent::Agent(repast::AgentId id, bool sex, int age, int drinking, int drinkFrequencyLevel, int monthlyDrinks, double monthlyDrinksSDPct){
	mId = id;
	mSex = sex;
	mAge = age;
	mIsDrinkingToday = drinking;
	mNumberDrinksToday = drinking;
	mIs12MonthDrinker = drinking;
	mDrinkFrequencyLevel = drinkFrequencyLevel;
	initPastYearDrinks(monthlyDrinks, monthlyDrinksSDPct);

	initDispositions();
}


void Agent::setMediator(TheoryMediator *mediator) {
    mpMediator = mediator;
    mpMediator->linkAgent(this);
}

Agent::~Agent(){
	delete mpMediator;
}

// Initialise vector of dispositions
void Agent::initDispositions(){
	int dis = 0;
	for(dis=0; dis<MAX_DRINKS; dis++)
	{
		mDispositions.push_back(0.0);
	}
}


void Agent::set(int currentRank, int age, bool sex, bool currentDrinking, int currentQuantity){
    mId.currentRank(currentRank);
    mAge = age;
    mSex = sex;
    mIsDrinkingToday = currentDrinking;
    mNumberDrinksToday = currentQuantity;
}

void Agent::setDispositionByIndex(int index, double value) {
	mDispositions[index] = value;
}


// Apply situational mechanisms
void Agent::doSituation() {
	mpMediator->mediateSituation();
}

// Calculate disposition vector from theories
void Agent::doDisposition() {
	mpMediator->mediateGatewayDisposition();
	mpMediator->mediateNextDrinksDisposition();
}

// Use drinking engine to calculate the number of drinks
void Agent::doDrinkingEngine() {
    bool stillDrinking = true;
	int numberOfDrinks = 0;
	do {
		if(repast::Random::instance()->nextDouble() > mDispositions[numberOfDrinks]){
			stillDrinking = false;
		} else{
			numberOfDrinks++;
		}
	} while (stillDrinking == true && numberOfDrinks < MAX_DRINKS);
	
	mIsDrinkingToday = (numberOfDrinks > 0);
	mNumberDrinksToday = numberOfDrinks;

	if (mIsDrinkingToday) {mIs12MonthDrinker = true;} //turn on the 12-month drinker flag if this agent drinks
}

// Apply action mechanisms
void Agent::doAction() {
	mpMediator->mediateNonDrinkingActions();
	doDisposition();
	doDrinkingEngine();
	updatePastYearDrinks();
}

// Age the agent by one year (assume all agents age at the same time)
void Agent::ageAgent(){
	mAge++;
}

void Agent::reset12MonthDrinker() {
	mIs12MonthDrinker = false;
}

// method to identify age group of the agent ranging from 0 to NUM_AGE_GROUPS -1
int Agent::findAgeGroup() {
	int myAgeGroup = 0;

	if (mAge <= MAX_AGE) {
		while (mAge > AGE_GROUPS[myAgeGroup]) {
			myAgeGroup++;
		}
	} else {
		myAgeGroup = NUM_AGE_GROUPS - 1;
	}

	return myAgeGroup; // Age group 0 is the youngest age groups
}

bool   Agent::isHaveKDrinksOverNDays(int numberOfDays, int kNumberDrinks) {
	bool nDaysKDrinking = false;
	int index = mPastYearDrinks.size()-1;
	while (numberOfDays > 0) {
		if (mPastYearDrinks[index] >= kNumberDrinks){
			nDaysKDrinking = true;
		}
		index--;
		numberOfDays--;
	}
	return nDaysKDrinking;
}

double  Agent::getAvgDrinksNDays(int numberOfDays){
	double averageDrinks = 0;
	int totalDays = numberOfDays;
	int index = mPastYearDrinks.size()-1;
	while (numberOfDays > 0) {
		averageDrinks = averageDrinks + mPastYearDrinks[index];
		index--;
		numberOfDays--;
	}
	return averageDrinks/totalDays;
}

double  Agent::getAvgDrinksNDays(int numberOfDays, bool perOccasion){
	double averageDrinks = 0;
	int index = mPastYearDrinks.size()-1;
	int totalDays;
	if (perOccasion == false) {
		totalDays = numberOfDays;
		while (numberOfDays > 0) {
			averageDrinks = averageDrinks + mPastYearDrinks[index];
			index--;
			numberOfDays--;
		}
	} else {
		totalDays = 0;
		while (numberOfDays > 0) {
			if (mPastYearDrinks[index] > 0) {
			averageDrinks = averageDrinks + mPastYearDrinks[index];
			totalDays++;
		}
		index--;
		numberOfDays--;
	}

	}
	if (totalDays == 0){
		averageDrinks = 0;
	} else {
		averageDrinks = averageDrinks/totalDays;
	}

	return averageDrinks;
}


int Agent::getNumDaysHavingKDrinksOverNDays(int numberOfDays, int kNumberDrinks) {
	int countDays = 0;
	int index = mPastYearDrinks.size()-1;
	while (numberOfDays > 0) {
		if (mPastYearDrinks[index] >= kNumberDrinks){
			countDays++;
		}
		index--;
		numberOfDays--;
	}
	return countDays;
}


//shuffle a list using Fisher-Yates shuffle
template<typename T>
void Agent::shuffleList(std::vector<T>& elementList){
  if(elementList.size() <= 1) return;
  repast::DoubleUniformGenerator rnd = repast::Random::instance()->createUniDoubleGenerator(0, 1);
  T swap;
  for(int pos = elementList.size() - 1; pos > 0; pos--){
	  int range = pos + 1;
	  int other = (int)(rnd.next() * (range));
	  swap = elementList[pos];
	  elementList[pos] = elementList[other];
	  elementList[other] = swap;
  }
}

//generates and returns the mPastYearDrinks vector based on drink frequency data, monthly drink amount data,
//at initialization, there is no variation in the quantity consumed.  FOR NOW.  Since this number will be averaged anyway, it's OK for it 
//not to wiggle at the outset.  Right?
void Agent::initPastYearDrinks(int monthlyDrinks, double monthlyDrinksSDPct){
	int drinkLevel = mDrinkFrequencyLevel;
	double doubleMonthlyDrinks = double (monthlyDrinks);

	int pastYearDrinksLength; //number of non-negative entries in pastYearDrinks vector.  365 for everyday, 0 for abstinence
	int maxDrinksPerCategory = 0;
	std::vector<int> pastYearDrinks;

	switch(drinkLevel){
		case 1:
			if (mIs12MonthDrinker == false){
				pastYearDrinksLength = 0;
			}else {
				pastYearDrinksLength = 1 + repast::Random::instance()->nextDouble() * 10;
				mDrinkFrequencyLevel = 2;
			}
			break;
		case 2:
			pastYearDrinksLength = 11 + repast::Random::instance()->nextDouble() * 7;
			maxDrinksPerCategory = 18;
			break;
		case 3:
			pastYearDrinksLength = 19 + repast::Random::instance()->nextDouble() * 21;
			maxDrinksPerCategory = 30;
			break;
		case 4:
			pastYearDrinksLength = 31 + repast::Random::instance()->nextDouble() * 22;
			maxDrinksPerCategory = 53;
			break;
		case 5:
			pastYearDrinksLength = 54 + repast::Random::instance()->nextDouble() * 117;
			maxDrinksPerCategory = 171;
			break;
		case 6:
			pastYearDrinksLength = 172 + repast::Random::instance()->nextDouble() * 164;
			maxDrinksPerCategory = 336;
			break;
		case 7:
			pastYearDrinksLength = 337 + repast::Random::instance()->nextDouble() * 28;
			maxDrinksPerCategory = 365;
			break;
	}
	if (pastYearDrinksLength == 0){                                  //nondrinkers get a year of 0
		std::vector<int> pastYearDrinksTemp(365, 0);
		pastYearDrinks = pastYearDrinksTemp;
	}else if (mDrinkFrequencyLevel > 1 && doubleMonthlyDrinks == 0){ //0 past month drinks >0 past year drinking, 1 for all drinking days
		int fillers = 365 - pastYearDrinksLength;
		int counter = pastYearDrinksLength;
		std::vector<int> pastYearDrinksTemp(fillers, 0);
		pastYearDrinks = pastYearDrinksTemp;
		while (counter != 0){
			pastYearDrinks.push_back(1);
			--counter;
		}
	//	std::cout << "I drink VERY VERY little" << std::endl;
	}else{
		double meanDrinksToday = doubleMonthlyDrinks / double(pastYearDrinksLength) * 12 ;
		double drinksSD = meanDrinksToday * monthlyDrinksSDPct;
		repast::NormalGenerator normGen = repast::Random::instance()->createNormalGenerator(meanDrinksToday, drinksSD);
		int yearCounter = 0;
		int quantityCounter = 0;
		while (yearCounter != 365){
			if (pastYearDrinksLength == quantityCounter){
				pastYearDrinks.push_back(0);
			}else{
				double doubleDrinksToday = normGen.next();
				int numDrinksToday = round(doubleDrinksToday);
				if (numDrinksToday<0) {
					numDrinksToday=0;
				}
				pastYearDrinks.push_back(numDrinksToday);
				++quantityCounter;
			}
			++yearCounter;
		}
	}

	shuffleList(pastYearDrinks);
	mPastYearDrinks = pastYearDrinks;

	//calculate mean and variance from history using Welford method
	mPastYearN = 0;
	mPastYearMeanDrink = 0;
	mPastYearSquaredDistanceDrink = 0;
	double oldMean = 0;
	for (int k=1; k<=365; k++) { //length of past-year-drink vector must be 365
		int x = pastYearDrinks[k-1];
		if (x!=0) { //only account for drinking days (number of drinks > 0)
			mPastYearN++;
			oldMean = mPastYearMeanDrink;
			mPastYearMeanDrink = mPastYearMeanDrink + (x - mPastYearMeanDrink) / mPastYearN;
			mPastYearSquaredDistanceDrink = mPastYearSquaredDistanceDrink + (x - mPastYearMeanDrink)*(x-oldMean);
		}
	}
}

void Agent::updatePastYearDrinks(){
	if (mIsDrinkingToday == true){
		mPastYearDrinks.push_back(mNumberDrinksToday);
		updateForwardMeanVariance(mNumberDrinksToday);
	}else{
		mPastYearDrinks.push_back(0);
	}

	if ( mPastYearDrinks.front() != 0 ) {
		updateBackwardMeanVariance(mPastYearDrinks.front());
	}

	mPastYearDrinks.erase(mPastYearDrinks.begin());
}

//forward update past-year mean and variance using Welford method
void Agent::updateForwardMeanVariance(int addedValue) {
	mPastYearN++;
	if (mPastYearN != 0) {
		double oldMean = mPastYearMeanDrink;
		mPastYearMeanDrink = mPastYearMeanDrink + (addedValue - mPastYearMeanDrink) / mPastYearN;
		mPastYearSquaredDistanceDrink = mPastYearSquaredDistanceDrink + (addedValue - oldMean)*(addedValue - mPastYearMeanDrink);
	} else {
		mPastYearMeanDrink = 0;
		mPastYearSquaredDistanceDrink = 0;
	}

	//limit estimated-mean between 0 and MAX_DRINKS
	if (mPastYearMeanDrink < 0) {mPastYearMeanDrink = 0;}
	if (mPastYearMeanDrink > MAX_DRINKS) {mPastYearMeanDrink = MAX_DRINKS;}
}

//backward update past-year mean and variance using Welford method
void Agent::updateBackwardMeanVariance(int removedValue) {
	mPastYearN--;
	if (mPastYearN != 0) {
		double oldMean = mPastYearMeanDrink;
		mPastYearMeanDrink = ((mPastYearN+1)*mPastYearMeanDrink - removedValue) / mPastYearN;
		mPastYearSquaredDistanceDrink = mPastYearSquaredDistanceDrink - (removedValue - oldMean)*(removedValue - mPastYearMeanDrink);
	} else {
		mPastYearMeanDrink = 0;
		mPastYearSquaredDistanceDrink = 0;
	}

	//limit estimated-mean between 0 and MAX_DRINKS
	if (mPastYearMeanDrink < 0) {mPastYearMeanDrink = 0;}
	if (mPastYearMeanDrink > MAX_DRINKS) {mPastYearMeanDrink = MAX_DRINKS;}
}

/* Serializable Agent Package Data */

AgentPackage::AgentPackage(){ }

AgentPackage::AgentPackage(int _id, int _rank, int _type, int _currentRank, int _age, bool _sex, bool _isDrinkingToday, int _numberDrinksToday):
id(_id), rank(_rank), type(_type), currentRank(_currentRank), age(_age), sex(_sex), isDrinkingToday(_isDrinkingToday), 
numberDrinksToday(_numberDrinksToday){ }

AgentPackage::AgentPackage(int _id, int _rank, int _type, int _currentRank, int _age, bool _sex, bool _isDrinkingToday, 
	int _numberDrinksToday, int _drinkFrequencyLevel, std::vector<int> _pastYearDrinks):
id(_id), rank(_rank), type(_type), currentRank(_currentRank), age(_age), sex(_sex), isDrinkingToday(_isDrinkingToday), 
numberDrinksToday(_numberDrinksToday), drinkFrequencyLevel(_drinkFrequencyLevel), pastYearDrinks(_pastYearDrinks){ }


/* SPECIFIC TO CONTAGION */
void Agent::set(int currentRank, int age, bool sex, bool currentDrinking,
				int currentQuantity, int currentFrequency, std::vector<int> pastYearDrinks){
    mId.currentRank(currentRank);
    mAge = age;
    mSex = sex;
    mIsDrinkingToday = currentDrinking;
    mNumberDrinksToday = currentQuantity;
    mDrinkFrequencyLevel = currentFrequency;
    mPastYearDrinks = pastYearDrinks;
}

void Agent::setDrinkFrequencyLevel(int drinkFrequencyLevel){
	mDrinkFrequencyLevel = drinkFrequencyLevel;
}
