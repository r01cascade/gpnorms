#include "vector"
#include "globals.h"

#include "RegulatorInjunctiveBingePunishment.h"

RegulatorInjunctiveBingePunishment::RegulatorInjunctiveBingePunishment(repast::SharedContext<Agent> *context) {
	mpContext = context;

	//create a 2D array: noRow x noCol
	adjustmentLevelsGamma = new double*[NUM_SEX];
	adjustmentLevelsLambda = new double*[NUM_SEX];
	mTransformationalTriggerCount = new int*[NUM_SEX];
	for(int i = 0; i < NUM_SEX; ++i) {
		adjustmentLevelsGamma[i] = new double[NUM_AGE_GROUPS];
		adjustmentLevelsLambda[i] = new double[NUM_AGE_GROUPS];
		mTransformationalTriggerCount[i] = new int[NUM_AGE_GROUPS];
	}

	resetCount();
}

RegulatorInjunctiveBingePunishment::~RegulatorInjunctiveBingePunishment() {
	for(int i = 0; i < NUM_SEX; ++i) {
		delete[] adjustmentLevelsGamma[i];
		delete[] adjustmentLevelsLambda[i];
		delete[] mTransformationalTriggerCount[i];
	}
	delete[] adjustmentLevelsGamma;
	delete[] adjustmentLevelsLambda;
	delete[] mTransformationalTriggerCount;
}

void RegulatorInjunctiveBingePunishment::resetCount() {
	for (int i=0; i<NUM_SEX; ++i)
		for (int j=0; j<NUM_AGE_GROUPS; ++j)
			mTransformationalTriggerCount[i][j] = 0;
}

void RegulatorInjunctiveBingePunishment::resetAdjustmentLevel() {
	for (int i=0; i<NUM_SEX; ++i)
		for (int j=0; j<NUM_AGE_GROUPS; ++j) {
			adjustmentLevelsGamma[i][j] = 1;
			adjustmentLevelsLambda[i][j] = 1;
		}
}

void RegulatorInjunctiveBingePunishment::updateAdjustmentLevel() {
	std::vector<Agent*> agents;
	mpContext->selectAgents(mpContext->size(), agents);

	//init array, reset to 0
	int countN [NUM_SEX][NUM_AGE_GROUPS];
	int countHeavyDrinker [NUM_SEX][NUM_AGE_GROUPS];
	for (int i=0; i<NUM_SEX; ++i)
		for (int j=0; j<NUM_AGE_GROUPS; ++j) {
			countN[i][j] = 0;
			countHeavyDrinker[i][j] = 0;
		}

	//loop through agent and count num of heavy drinkers
	std::vector<Agent*>::const_iterator iter = agents.begin();
	std::vector<Agent*>::const_iterator iterEnd = agents.end();
	while (iter != iterEnd) {
		++countN[(*iter)->getSex()][(*iter)->findAgeGroup()];
		if ((*iter)->getAvgDrinksNDays(COMP_DAYS_PUNISH, false)>INJUNCTIVE_THRESHOLD) {
			++countHeavyDrinker[(*iter)->getSex()][(*iter)->findAgeGroup()];
		}
		iter++;
	}

	//update adjustment levels
	for (int i=0; i<NUM_SEX; ++i)
		for (int j=0; j<NUM_AGE_GROUPS; ++j) {
			double proportionHeavyDrinkers;
			if (countN[i][j] !=0){
				proportionHeavyDrinkers = double(countHeavyDrinker[i][j]) / double(countN[i][j]);
			} else {
				//std::cout << "There are no people in this demographic here" << std::endl;
			}

			if (proportionHeavyDrinkers > INJUNCTIVE_PROPORTION) {
				mTransformationalTriggerCount[i][j]++;
				adjustmentLevelsGamma[i][j] = INJ_PUNISHMENT_GAMMA_ADJUSTMENT;
				adjustmentLevelsLambda[i][j] = INJ_PUNISHMENT_LAMBDA_ADJUSTMENT;
				//std::cout << "The injunctive norm was adjusted (punish): " << i << " " << j << std::endl;
			} else {
				adjustmentLevelsGamma[i][j] = 1;
				adjustmentLevelsLambda[i][j] = 1;
			}
		}

/* #ifdef DEBUG
			std::stringstream msg;
			msg
					<< "Heavy drinkers had to be punished. The new injunctive norm of this group is now: "
					<< mpInjunctiveNorms[currentSex][currentAgeGroup]
					<< "\n";
			std::cout << msg.str();
#endif */

}

double RegulatorInjunctiveBingePunishment::getAdjustmentLevelGamma(int sex, int ageGroup) {
	return adjustmentLevelsGamma[sex][ageGroup];
}

double RegulatorInjunctiveBingePunishment::getAdjustmentLevelLambda(int sex, int ageGroup) {
	return adjustmentLevelsLambda[sex][ageGroup];
}
