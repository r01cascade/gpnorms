# load libraries
library(ggplot2)
library(reshape2)
library(stats)


# Define home directory, folders, and file names
#home_directory <- "C:/Users/Charlotte_Probst/Dropbox/CASCADE/norms_model/"
home_directory <- "/home/lotta/model/norms"
data_folder <- "/outputs"
out_folder <- "/postprocessing"
data_name <- "/agent_drinks_data"


setwd(home_directory)

source(paste(home_directory, out_folder, "/preprocess-data-output.R", sep = ""))

data <- read.csv(paste(home_directory, data_folder, "/",  "agent_drinks_data_processed.csv", sep = ""))
#data <- data[1:100,]
keepVar <- c("tick", "nMen",	'nWomen', "nTotal", "nAge0",	"nAge1",	"nAge2",	"nAge3",	"nAge4",	"nAge5",	"nAge6", "nAge7", "nAge8",
             "drinkersMen",	"drinkersWomen",	"drinkersTotal",
             "prevDrinkersMen",	"prevDrinkersWomen",	"prevDrinkersTotal",
             "drinkersAge0",	"drinkersAge1",	"drinkersAge2",	"drinkersAge3",	"drinkersAge4",	"drinkersAge5",	"drinkersAge6",	"drinkersAge7",	"drinkersAge8",
             "prevDrinkersAge0",	"prevDrinkersAge1",	"prevDrinkersAge2",	"prevDrinkersAge3",	"prevDrinkersAge4",	"prevDrinkersAge5",	"prevDrinkersAge6",	"prevDrinkersAge7",	"prevDrinkersAge8",
             "meanDrinksAllMen",	"meanDrinksAllWomen",	"meanDrinksAllTotal",	
             "meanDrinksCdMen", "meanDrinksCdWomen",	"meanDrinksCdTotal", 
             "meanDrinksAllAge0",	"meanDrinksAllAge1",	"meanDrinksAllAge2",	"meanDrinksAllAge3",	"meanDrinksAllAge4",	"meanDrinksAllAge5",	"meanDrinksAllAge6",	"meanDrinksAllAge7",	"meanDrinksAllAge8",	
             "meanDrinksCdAge0",	"meanDrinksCdAge1",	"meanDrinksCdAge2",	"meanDrinksCdAge3",	"meanDrinksCdAge4",	"meanDrinksCdAge5",	"meanDrinksCdAge6",	"meanDrinksCdAge7",	"meanDrinksCdAge8")
             
data <- data[, names(data) %in% keepVar]


# Melt data frame to facilitate plotting
# plot prevalence of drinking by sex
dataMolten <- melt(data[, c("tick", "prevDrinkersMen", "prevDrinkersWomen", "prevDrinkersTotal")], id.vars = "tick", variable.name = "series", value.name = "prevalence")
ggplot(dataMolten, aes(x = tick, y = prevalence, group = series, color = series)) + geom_line()
ggsave(paste(home_directory, out_folder, "prevalence_sex.jpeg", sep = ""), units = "cm", height = 5, width = 10, scale = 1.5)

spectrum(dataMolten[c(1:400),3])
write.csv(dataMolten, "dataMolten.csv")

# plot average drinks by sex
to_include <- c("tick", names(data[, c(findCol(data, "meanDrinksCdMen"):findCol(data, "meanDrinksCdTotal"))]) )
dataMolten <- melt(data[, to_include], id.vars = "tick", variable.name = "series", value.name = "averageDrinks")
ggplot(dataMolten, aes(x = tick, y = averageDrinks, group = series, color = series)) + geom_line() + scale_color_discrete(name="Sex", labels=c("Men", "Women", "Total"))
ggsave(paste(home_directory, out_folder, "drinks_age.jpeg", sep = ""), units = "cm", height = 5, width = 10, scale = 1.5)

# plot prevalence of drinking by age
to_include <- c("tick", names(data[, c(findCol(data, "prevDrinkersAge0"):findCol(data, "prevDrinkersAge8"))]) )
dataMolten <- melt(data[, to_include], id.vars = "tick", variable.name = "series", value.name = "prevalence")
ggplot(dataMolten, aes(x = tick, y = prevalence, group = series, color = series)) + geom_line() + scale_color_discrete(name="Age group", labels=c("<15", "15-24", "25-34", "35-44", "45-54", "55-64", 
                                                                                                                                                   "65-74", "75-84", "85+"))
ggsave(paste(home_directory, "model_output/", "prevalence_age.jpeg", sep = ""), units = "cm", height = 5, width = 10, scale = 1.5)

# plot average drinks among current drinkers by age
to_include <- c("tick", names(data[, c(findCol(data, "meanDrinksCdAge0"):findCol(data, "meanDrinksCdAge8"))]) )
dataMolten <- melt(data[, to_include], id.vars = "tick", variable.name = "series", value.name = "averageDrinks")
ggplot(dataMolten, aes(x = tick, y = averageDrinks, group = series, color = series)) + geom_line() + scale_color_discrete(name="Age group", labels=c("<15", "15-24", "25-34", "35-44", "45-54", "55-64", 
                                                                                                                                                     "65-74", "75-84", "85+"))
ggsave(paste(home_directory, "model_output/", "mean_drinks_age_cd.jpeg", sep = ""), units = "cm", height = 5, width = 10, scale = 1.5)



# plot number of agents over time by sex
to_include <- c("tick", names(data[, c(findCol(data, "nMen"):findCol(data, "nTotal"))]) )
dataMolten <- melt(data[, to_include], id.vars = "tick", variable.name = "series", value.name = "Population")
ggplot(dataMolten, aes(x = tick, y = Population, group = series, color = series)) + geom_line() + scale_color_discrete(name="Sex", labels=c("Men", "Women", "Total"))
ggsave(paste(home_directory, "model_output/", "population_sex.jpeg", sep = ""), units = "cm", height = 5, width = 10, scale = 1.5)

# plot number of agents over time by age
to_include <- c("tick", names(data[, c(findCol(data, "nAge0"):findCol(data, "nAge8"))]) )
dataMolten <- melt(data[, to_include], id.vars = "tick", variable.name = "series", value.name = "Population")
ggplot(dataMolten, aes(x = tick, y = Population, group = series, color = series)) + geom_line() + scale_color_discrete(name="Age group", labels=c("<15", "15-24", "25-34", "35-44", "45-54", "55-64", 
                                                                                                                                                     "65-74", "75-84", "85+"))
ggsave(paste(home_directory, "model_output/", "population_age.jpeg", sep = ""), units = "cm", height = 5, width = 10, scale = 1.5)


