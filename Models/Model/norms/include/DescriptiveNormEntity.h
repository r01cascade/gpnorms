#ifndef INCLUDE_DESCRIPTIVE_NORM_ENTITY_H_
#define INCLUDE_DESCRIPTIVE_NORM_ENTITY_H_

#include "StructuralEntity.h"
#include "repast_hpc/SharedContext.h"
#include "Agent.h"

class DescriptiveNormEntity : public StructuralEntity {

private:
	repast::SharedContext<Agent> *mpContext;
	double** mpDescriptiveNorms;

	double** mpAvgIsDrinking;
	double** mpAvgNumberDrinks;
	double** mpSdNumberDrinks;

public:
	DescriptiveNormEntity(std::vector<Regulator*> regulatorList, std::vector<double> powerList,
			int transformationalInterval,
			repast::SharedContext<Agent> *context);
	~DescriptiveNormEntity() {};
	void doTransformation() override;

	void updateDescriptiveGroupDrinking();
	double getAvgIsDrinking(int sex, int ageGroup);
	double getAvgNumberDrinks(int sex, int ageGroup);
	double getSdNumberDrinks(int sex, int ageGroup);
};

#endif /* INCLUDE_DESCRIPTIVE_NORM_ENTITY_H_ */
