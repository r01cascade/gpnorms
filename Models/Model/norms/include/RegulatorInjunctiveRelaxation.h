#ifndef INCLUDE_REGULATORINJUNCTIVERELAXATION_H_
#define INCLUDE_REGULATORINJUNCTIVERELAXATION_H_

#include "repast_hpc/SharedContext.h"

#include "Regulator.h"
#include "Agent.h"
#include "InjunctiveNormEntity.h"

class RegulatorInjunctiveRelaxation : public Regulator {

private:
	repast::SharedContext<Agent> *mpContext;
	double** adjustmentLevelsGamma;
	double** adjustmentLevelsLambda;
	InjunctiveNormEntity *mpInjunctiveNormEntity;

public:
	RegulatorInjunctiveRelaxation(repast::SharedContext<Agent> *context);
	~RegulatorInjunctiveRelaxation();

	void updateAdjustmentLevel() override;

	void setInjNormEntity(InjunctiveNormEntity *pInjNormEntity);

	double getAdjustmentLevelGamma(int sex, int ageGroup);
	double getAdjustmentLevelLambda(int sex, int ageGroup);

	int** mTransformationalTriggerCount;
	void resetCount();
	void resetAdjustmentLevel();
};


#endif /* INCLUDE_REGULATORINJUNCTIVERELAXATION_H_ */
