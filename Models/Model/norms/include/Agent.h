/* Agent.h */

#ifndef AGENT
#define AGENT

#include "globals.h"
#include "TheoryMediator.h"
#include "Theory.h"

#include "repast_hpc/AgentId.h"
#include "repast_hpc/SharedContext.h"

#include <cmath>

/* Agents */
class Agent {
	
private:
    repast::AgentId  mId;
    int              mAge;
    bool             mSex;
    bool       	     mIsDrinkingToday;
    int 			 mNumberDrinksToday;
    bool 			 mIs12MonthDrinker; //true if this agent has a drink in the last year. Reset every year.
    int              mDrinkFrequencyLevel;
    std::vector<int> mPastYearDrinks;
  
    TheoryMediator      *mpMediator;
    std::vector<double> mDispositions;

    void initDispositions(); //init dispositions array:size MAX_DRINK, all values 0
    void doDisposition(); //calculate disposition array from theories
    void doDrinkingEngine(); //calculate number of drinks
    static int myrandom (int i); //generator for random_shuffle

    int mPastYearN; //number of drinking days
    double mPastYearMeanDrink; //accumulates the mean of drinks within drinking days
    double mPastYearSquaredDistanceDrink; //aggregates the squared distance from the mean
    void updateForwardMeanVariance(int addedValue);
    void updateBackwardMeanVariance(int removedValue);

    template<typename T>
    void shuffleList(std::vector<T>& elementList); //shuffle a list using Fisher-Yates shuffle

public:
    Agent(repast::AgentId id);  // Constructor
    Agent(repast::AgentId id, bool sex, int age, int drinking, int drinkFrequencyLevel, int monthlyDrinks, double monthlyDrinksSDPct);
    virtual ~Agent(); // Destructor
	
    /* Required Getters */
    virtual repast::AgentId& getId(){                               return mId;                 }
    virtual const repast::AgentId& getId() const{                   return mId;                 }
	
    /* Getters specific to this kind of Agent */
    int    getAge(){                                                return mAge;                }
    bool   getSex(){                                                return mSex;                }
    bool   isDrinkingToday(){                                       return mIsDrinkingToday;    }
    int    getNumberDrinksToday(){						            return mNumberDrinksToday;  }
    bool   is12MonthDrinker() {						                return mIs12MonthDrinker;   }
    bool   isHaveKDrinksOverNDays(int numberOfDays, int kNumberDrinks);
    double getAvgDrinksNDays(int numberOfDays);
    double getAvgDrinksNDays(int numberOfDays, bool perOccasion);
    int    getNumDaysHavingKDrinksOverNDays(int numberOfDays, int kNumberDrinks);
    
    //give the address of a theory within this agent that matched the type of provided theory ppTheory,
    //return true if success.
    template <typename derivedTheory>
    bool getTheory(derivedTheory** ppTheory) {
    	mpMediator->getTheory(ppTheory);
    }
	//

    /* Setter */
    void set(int currentRank, int age, bool sex, bool currentDrinking, int currentQuantity);
    void setMediator(TheoryMediator *mediator);
    void setDispositionByIndex(int index, double value);
        
    /* Situational mechanisms */
    void doSituation();

    /* Action mechanisms */
    void doAction();

    // Make agent older (in years)
    void ageAgent();

    void reset12MonthDrinker();

    int findAgeGroup(); //TODO: decide to put age here or remove it

    void initPastYearDrinks(int monthlyDrinks, double monthlyDrinksSDPct);
    void updatePastYearDrinks();

    double getPastYearMeanDrink() { return mPastYearMeanDrink; }
	double getPastYearVarianceDrink() { return (mPastYearN<=1 ? 0 : mPastYearSquaredDistanceDrink / (mPastYearN-1)); }
	double getPastYearSdDrink() { return sqrt(getPastYearVarianceDrink()); }

	int    getDrinkFrequencyLevel(){        return mDrinkFrequencyLevel;}
    std::vector<int> getPastYearDrinks(){   return mPastYearDrinks;     }
    
    //CONTAGION
    void set(int currentRank, int age, bool sex, bool currentDrinking, int currentQuantity, int currentFrequency,
             std::vector<int> pastYearDrinks);
    void setDrinkFrequencyLevel(int drinkFrequencyLevel);

};

/* Serializable Agent Package */
struct AgentPackage{
	
public:
    int     id;
    int     rank;
    int     type;
    int     currentRank;
    int     age;
    bool    sex;
    bool    isDrinkingToday; // these are the properties that are observable by other agents which is all that is required by the norms model
	int 	numberDrinksToday;
    int     drinkFrequencyLevel; //this is observable by other agents and needed by the CONTAGION model
    std::vector<int> pastYearDrinks;

    /* Constructors */
    AgentPackage(); // For serialization
    AgentPackage(int _id, int _rank, int _type, int _currentRank, int _age, bool _sex, bool _isDrinkingToday, int _numberDrinksToday);
	AgentPackage(int _id, int _rank, int _type, int _currentRank, int _age, bool _sex, bool _isDrinkingToday, 
                 int _numberDrinksToday, int _drinkFrequencyLevel, std::vector<int> _pastYearDrinks);
    

    /* For archive packaging */
    template<class Archive>
    void serialize(Archive &ar, const unsigned int version){
        ar & id;
        ar & rank;
        ar & type;
        ar & currentRank;
        ar & age;
        ar & sex;
        ar & isDrinkingToday;
        ar & numberDrinksToday;
        ar & drinkFrequencyLevel;
        ar & pastYearDrinks;
    }
};


#endif
