#ifndef INCLUDE_REGULATOR_INJUNCTIVE_BINGE_PUNISHMENT_H_
#define INCLUDE_REGULATOR_INJUNCTIVE_BINGE_PUNISHMENT_H_

#include "repast_hpc/SharedContext.h"

#include "Regulator.h"
#include "Agent.h"

class RegulatorInjunctiveBingePunishment : public Regulator {

private:
	repast::SharedContext<Agent> *mpContext;
	double** adjustmentLevelsGamma;
	double** adjustmentLevelsLambda;

public:
	RegulatorInjunctiveBingePunishment(repast::SharedContext<Agent> *context);
	~RegulatorInjunctiveBingePunishment();

	void updateAdjustmentLevel() override;

	double getAdjustmentLevelGamma(int sex, int ageGroup);
	double getAdjustmentLevelLambda(int sex, int ageGroup);

	int** mTransformationalTriggerCount;
	void resetCount();
	void resetAdjustmentLevel();
};

#endif /* INCLUDE_REGULATOR_INJUNCTIVE_BINGE_PUNISHMENT_H_ */
