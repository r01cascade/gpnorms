#ifndef INCLUDE_INJUNCTIVE_NORM_ENTITY_H_
#define INCLUDE_INJUNCTIVE_NORM_ENTITY_H_

#include <string>

#include "StructuralEntity.h"

class InjunctiveNormEntity : public StructuralEntity {

private:
	double** mpInjunctiveNormsGate;
	double** mpInjunctiveNormsGamma;
	double** mpInjunctiveNormsLambda;
	int mIntervalPunish;
	int mIntervalRelax;

public:
	InjunctiveNormEntity(std::vector<Regulator*> regulatorList, std::vector<double> powerList,
			int intervalPunish, int intervalRelax,
			std::vector<std::string> normDataGate,
			std::vector<std::string> normDataGamma, std::vector<std::string> normDataLambda);
	~InjunctiveNormEntity();

	void doTransformation() override;
	double getInjNormGate(int sex, int ageGroup); //getter to read value from inj norm array
	double getInjNormGamma(int sex, int ageGroup); //getter to read value from inj norm array
	double getInjNormLambda(int sex, int ageGroup); //getter to read value from inj norm array
};

#endif /* INCLUDE_INJUNCTIVE_NORM_ENTITY_H_ */
