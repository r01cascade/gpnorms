#ifndef INCLUDE_THEORY_H_
#define INCLUDE_THEORY_H_

class Agent; //forward declare, but DO NOT define

#include <utility>

class Theory {

protected:
	Agent *mpAgent;

public:
	virtual ~Theory() {};

	void setAgent(Agent *agent);
	virtual void doSituation() = 0;
	virtual void doGatewayDisposition() = 0;
	virtual void doNextDrinksDisposition() = 0;
	virtual void doNonDrinkingActions() = 0;

	//correct mean and sd for ERFC function
	std::pair<double, double> generateCorrectedMeanSD (double desiredMean, double desiredSd);
	std::pair<double, double> doLookup(double mean, double sd);
	std::pair<double, double> doFunctionLookup(double desiredMean, double desiredSd);
};

#endif /* INCLUDE_THEORY_H_ */
