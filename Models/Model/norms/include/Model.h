/* Model.h */

#ifndef INCLUDE_MODEL_H_
#define INCLUDE_MODEL_H_

#include <boost/mpi.hpp>
#include "repast_hpc/Schedule.h"
#include "repast_hpc/Properties.h"
#include "repast_hpc/SharedContext.h"
#include "repast_hpc/AgentRequest.h"
#include "repast_hpc/TDataSource.h"
#include "repast_hpc/SVDataSet.h"
#include <chrono>

#include "Agent.h"
#include "DataOut.h"
#include "StructuralEntity.h"

/* Agent Package Provider */
class AgentPackageProvider {
	
private:
    repast::SharedContext<Agent>* agents;
	
public:
	
    AgentPackageProvider(repast::SharedContext<Agent>* agentPtr);
	
    void providePackage(Agent * agent, std::vector<AgentPackage>& out);
	
    void provideContent(repast::AgentRequest req, std::vector<AgentPackage>& out);
	
};

/* Agent Package Receiver */
class AgentPackageReceiver {
	
private:
    repast::SharedContext<Agent>* agents;
	
public:
	
    AgentPackageReceiver(repast::SharedContext<Agent>* agentPtr);
	
    Agent * createAgent(AgentPackage package);
	
    void updateAgent(AgentPackage package);
	
};


class Model {
private:
	int stopAt;
	const int MECHANISM_START_TICK = 1;
	const int MECHANISM_INTERVAL_TICK = 1;
	const int YEARLY_START_TICK = 0;
	const int YEARLY_INTERVAL_TICK = 365;
	int SITUATIONAL_MECHANISM_INTERVAL_TICK = 1; //read from model.props
	int ACTION_MECHANISM_INTERVAL_TICK = 1; //read from model.props
	AgentPackageProvider* provider;
	AgentPackageReceiver* receiver;
	std::chrono::steady_clock::time_point startTime; //for timeout ERROR

	int mIndexSex = -1;
	int mIndexAge = -1;
	int mIndexDrinking = -1;
	int mIndexFrequencyLevel = -1;
	int mIndexMonthlyDrinks = -1;
	int mIndexSpawnTick = -1;
	void getReadyToSpawn();
	int countDiePerYear = 0;
	int countSpawnPerYear = 0;

	void readMeanSDLookupTable(std::string lookupFilename);

protected:
	int countOfAgents;
	repast::Properties* props;
	repast::SharedContext<Agent> context;
	repast::SVDataSet* annualValues;
	std::vector<StructuralEntity*> structuralEntityList; //a list of structural entity that perform transformational mechanisms
	std::queue<std::vector<std::string>> infoTable; //to store all individual info from file
	std::queue<std::vector<std::vector<double>>> mortalityRates; //to yearly mortalityData, by sex & age group
	
	std::vector<std::string> readCSVRow(const std::string &row);
	std::queue<std::vector<std::string>> readCSV(std::istream &in);

	int mIndexId = -1; //so spawning can used this to check for id
	int simYear;

public:
	Model(std::string propsFile, int argc, char** argv, boost::mpi::communicator* comm);
	virtual ~Model();
	void initAgents(); //create agents
	void initSchedule(repast::ScheduleRunner& runner); //schedule 3 mechanisms and microsim time-related activities

	//these functions are ONLY IMPLEMENTED by (theory-specific) derived class
	virtual void initMediatorAndTheoryWithRandomParameters(Agent *agent) = 0; //for an agent, create theory-specific mediator & theories with random variables
	virtual void initMediatorAndTheoryFromFile(Agent *agent, std::vector<std::string> info) = 0; //for an agent, create theory-specific mediator & theories with variables read from file
	virtual void initForTheory(repast::ScheduleRunner& runner) = 0; //init and schedule data collection
	virtual void readRankFileForTheory(std::string rankFileName) = 0;
	virtual void connectSpawnAgentToNetwork(Agent* agent) = 0;
	virtual void initNetwork() = 0;
	virtual void removeLocalCopies(repast::AgentId agentId) = 0;
	virtual void doYearlyTheoryActions() = 0;

	void doDailyActions();
	void doYearlyActions();

	void doSituationalMechanisms();
	void doActionMechanisms();
	void doTransformationalMechanisms();
	
	void ageAgents(); // age all agents
	void spawnAgents(); // bring new adults into simulation.
	void killAgents(); // apply mortality rates to agents
	void incrementSimYear(); // go to the next year of simulation
	void reset12MonthDrinkers(); //reset 12-month drinking flag in all agents
};

#endif
