#ifndef INCLUDE_DATAOUT_H_
#define INCLUDE_DATAOUT_H_
#include "repast_hpc/TDataSource.h"
#include "repast_hpc/SVDataSet.h"
#include "repast_hpc/SVDataSetBuilder.h"
#include "Agent.h"
#include "repast_hpc/SharedContext.h"
#include "globals.h"

/* Data Collection */

//Agent population (total)
class OutSumPopulation : public repast::TDataSource<int>{
private:
	repast::SharedContext<Agent>* mpPopulation;

public:
	OutSumPopulation(repast::SharedContext<Agent>* pPopulation);
	int getData();
};

//Sum of male
class OutSumMale : public repast::TDataSource<int>{
private:
	repast::SharedContext<Agent>* mpPopulation;

public:
	OutSumMale(repast::SharedContext<Agent>* pPopulation);
	int getData();
};

//Sum of female
class OutSumFemale : public repast::TDataSource<int>{
private:
	repast::SharedContext<Agent>* mpPopulation;

public:
	OutSumFemale(repast::SharedContext<Agent>* pPopulation);
	int getData();
};

//Sum of people in age group 1: 12-17
class OutSumAgeGroup1 : public repast::TDataSource<int>{
private:
	repast::SharedContext<Agent>* mpPopulation;

public:
	OutSumAgeGroup1(repast::SharedContext<Agent>* pPopulation);
	int getData();
};

//Sum of people in age group 2: 18-34
class OutSumAgeGroup2 : public repast::TDataSource<int>{
private:
	repast::SharedContext<Agent>* mpPopulation;

public:
	OutSumAgeGroup2(repast::SharedContext<Agent>* pPopulation);
	int getData();
};

//Sum of people in age group 3: 35-64
class OutSumAgeGroup3 : public repast::TDataSource<int>{
private:
	repast::SharedContext<Agent>* mpPopulation;

public:
	OutSumAgeGroup3(repast::SharedContext<Agent>* pPopulation);
	int getData();
};

//Sum of people in age group 4: 65+
class OutSumAgeGroup4 : public repast::TDataSource<int>{
private:
	repast::SharedContext<Agent>* mpPopulation;

public:
	OutSumAgeGroup4(repast::SharedContext<Agent>* pPopulation);
	int getData();
};


//Number of drinkers in the current year
class OutSum12MonthDrinkers : public repast::TDataSource<int>{
private:
	repast::SharedContext<Agent>* mpPopulation;

public:
	OutSum12MonthDrinkers(repast::SharedContext<Agent>* pPopulation);
	int getData();
};

//Sum of male drinkers in the current year
class OutSum12MonthDrinkersMale : public repast::TDataSource<int>{
private:
	repast::SharedContext<Agent>* mpPopulation;

public:
	OutSum12MonthDrinkersMale(repast::SharedContext<Agent>* pPopulation);
	int getData();
};

//Sum of female drinkers in the current year
class OutSum12MonthDrinkersFemale : public repast::TDataSource<int>{
private:
	repast::SharedContext<Agent>* mpPopulation;

public:
	OutSum12MonthDrinkersFemale(repast::SharedContext<Agent>* pPopulation);
	int getData();
};

//Sum of drinkers in age group 1: 12-17
class OutSum12MonthDrinkersAgeGroup1 : public repast::TDataSource<int>{
private:
	repast::SharedContext<Agent>* mpPopulation;

public:
	OutSum12MonthDrinkersAgeGroup1(repast::SharedContext<Agent>* pPopulation);
	int getData();
};

//Sum of drinkers in age group 1: 18-34
class OutSum12MonthDrinkersAgeGroup2 : public repast::TDataSource<int>{
private:
	repast::SharedContext<Agent>* mpPopulation;

public:
	OutSum12MonthDrinkersAgeGroup2(repast::SharedContext<Agent>* pPopulation);
	int getData();
};

//Sum of drinkers in age group 1: 35-64
class OutSum12MonthDrinkersAgeGroup3 : public repast::TDataSource<int>{
private:
	repast::SharedContext<Agent>* mpPopulation;

public:
	OutSum12MonthDrinkersAgeGroup3(repast::SharedContext<Agent>* pPopulation);
	int getData();
};

//Sum of drinkers in age group 1: 65+
class OutSum12MonthDrinkersAgeGroup4 : public repast::TDataSource<int>{
private:
	repast::SharedContext<Agent>* mpPopulation;

public:
	OutSum12MonthDrinkersAgeGroup4(repast::SharedContext<Agent>* pPopulation);
	int getData();
};


// Quantity of male
class OutSumQuantMale : public repast::TDataSource<double>{
private:
	repast::SharedContext<Agent>* mpPopulation;

public:
	OutSumQuantMale(repast::SharedContext<Agent>* pPopulation);
	double getData();
};

// Quantity of female
class OutSumQuantFemale : public repast::TDataSource<double>{
private:
	repast::SharedContext<Agent>* mpPopulation;

public:
	OutSumQuantFemale(repast::SharedContext<Agent>* pPopulation);
	double getData();
};

// Quantity of age group 1
class OutSumQuantAgeGroup1 : public repast::TDataSource<double>{
private:
	repast::SharedContext<Agent>* mpPopulation;

public:
	OutSumQuantAgeGroup1(repast::SharedContext<Agent>* pPopulation);
	double getData();
};

// Quantity of age group 2
class OutSumQuantAgeGroup2 : public repast::TDataSource<double>{
private:
	repast::SharedContext<Agent>* mpPopulation;

public:
	OutSumQuantAgeGroup2(repast::SharedContext<Agent>* pPopulation);
	double getData();
};

// Quantity of age group 3
class OutSumQuantAgeGroup3 : public repast::TDataSource<double>{
private:
	repast::SharedContext<Agent>* mpPopulation;

public:
	OutSumQuantAgeGroup3(repast::SharedContext<Agent>* pPopulation);
	double getData();
};

// Quantity of age group 4
class OutSumQuantAgeGroup4 : public repast::TDataSource<double>{
private:
	repast::SharedContext<Agent>* mpPopulation;

public:
	OutSumQuantAgeGroup4(repast::SharedContext<Agent>* pPopulation);
	double getData();
};


// Frequency of male
class OutSumFreqMale : public repast::TDataSource<int>{
private:
	repast::SharedContext<Agent>* mpPopulation;

public:
	OutSumFreqMale(repast::SharedContext<Agent>* pPopulation);
	int getData();
};

// Frequency of female
class OutSumFreqFemale : public repast::TDataSource<int>{
private:
	repast::SharedContext<Agent>* mpPopulation;

public:
	OutSumFreqFemale(repast::SharedContext<Agent>* pPopulation);
	int getData();
};

// Frequency of age group 1
class OutSumFreqAgeGroup1 : public repast::TDataSource<int>{
private:
	repast::SharedContext<Agent>* mpPopulation;

public:
	OutSumFreqAgeGroup1(repast::SharedContext<Agent>* pPopulation);
	int getData();
};

// Frequency of age group 2
class OutSumFreqAgeGroup2 : public repast::TDataSource<int>{
private:
	repast::SharedContext<Agent>* mpPopulation;

public:
	OutSumFreqAgeGroup2(repast::SharedContext<Agent>* pPopulation);
	int getData();
};

// Frequency of age group 3
class OutSumFreqAgeGroup3 : public repast::TDataSource<int>{
private:
	repast::SharedContext<Agent>* mpPopulation;

public:
	OutSumFreqAgeGroup3(repast::SharedContext<Agent>* pPopulation);
	int getData();
};

// Frequency of age group 4
class OutSumFreqAgeGroup4 : public repast::TDataSource<int>{
private:
	repast::SharedContext<Agent>* mpPopulation;

public:
	OutSumFreqAgeGroup4(repast::SharedContext<Agent>* pPopulation);
	int getData();
};


// Total number of 5+ drinks days over the last 30 days
class OutSumOccasionalHeavyDrinking : public repast::TDataSource<int>{
private:
	repast::SharedContext<Agent>* mpPopulation;

public:
	OutSumOccasionalHeavyDrinking(repast::SharedContext<Agent>* pPopulation);
	int getData();
};

/*
// Sum of agents in each age * sex group
class OutSumMenAge0 : public repast::TDataSource<int>{
private:
	repast::SharedContext<Agent>* mpPopulation;

public:
	OutSumMenAge0(repast::SharedContext<Agent>* pPopulation);
	int getData();
};


class OutSumMenAge1 : public repast::TDataSource<int>{
private:
	repast::SharedContext<Agent>* mpPopulation;

public:
	OutSumMenAge1(repast::SharedContext<Agent>* pPopulation);
	int getData();
};

class OutSumMenAge2 : public repast::TDataSource<int>{
private:
	repast::SharedContext<Agent>* mpPopulation;

public:
	OutSumMenAge2(repast::SharedContext<Agent>* pPopulation);
	int getData();
};


class OutSumMenAge3 : public repast::TDataSource<int>{
private:
	repast::SharedContext<Agent>* mpPopulation;

public:
	OutSumMenAge3(repast::SharedContext<Agent>* pPopulation);
	int getData();
};


class OutSumMenAge4 : public repast::TDataSource<int>{
private:
	repast::SharedContext<Agent>* mpPopulation;

public:
	OutSumMenAge4(repast::SharedContext<Agent>* pPopulation);
	int getData();
};


class OutSumMenAge5 : public repast::TDataSource<int>{
private:
	repast::SharedContext<Agent>* mpPopulation;

public:
	OutSumMenAge5(repast::SharedContext<Agent>* pPopulation);
	int getData();
};


class OutSumMenAge6 : public repast::TDataSource<int>{
private:
	repast::SharedContext<Agent>* mpPopulation;

public:
	OutSumMenAge6(repast::SharedContext<Agent>* pPopulation);
	int getData();
};


class OutSumMenAge7 : public repast::TDataSource<int>{
private:
	repast::SharedContext<Agent>* mpPopulation;

public:
	OutSumMenAge7(repast::SharedContext<Agent>* pPopulation);
	int getData();
};


class OutSumMenAge8 : public repast::TDataSource<int>{
private:
	repast::SharedContext<Agent>* mpPopulation;

public:
	OutSumMenAge8(repast::SharedContext<Agent>* pPopulation);
	int getData();
};


class OutSumWomenAge0 : public repast::TDataSource<int>{
private:
	repast::SharedContext<Agent>* mpPopulation;

public:
	OutSumWomenAge0(repast::SharedContext<Agent>* pPopulation);
	int getData();
};


class OutSumWomenAge1 : public repast::TDataSource<int>{
private:
	repast::SharedContext<Agent>* mpPopulation;

public:
	OutSumWomenAge1(repast::SharedContext<Agent>* pPopulation);
	int getData();
};


class OutSumWomenAge2 : public repast::TDataSource<int>{
private:
	repast::SharedContext<Agent>* mpPopulation;

public:
	OutSumWomenAge2(repast::SharedContext<Agent>* pPopulation);
	int getData();
};


class OutSumWomenAge3 : public repast::TDataSource<int>{
private:
	repast::SharedContext<Agent>* mpPopulation;

public:
	OutSumWomenAge3(repast::SharedContext<Agent>* pPopulation);
	int getData();
};


class OutSumWomenAge4 : public repast::TDataSource<int>{
private:
	repast::SharedContext<Agent>* mpPopulation;

public:
	OutSumWomenAge4(repast::SharedContext<Agent>* pPopulation);
	int getData();
};


class OutSumWomenAge5 : public repast::TDataSource<int>{
private:
	repast::SharedContext<Agent>* mpPopulation;

public:
	OutSumWomenAge5(repast::SharedContext<Agent>* pPopulation);
	int getData();
};


class OutSumWomenAge6 : public repast::TDataSource<int>{
private:
	repast::SharedContext<Agent>* mpPopulation;

public:
	OutSumWomenAge6(repast::SharedContext<Agent>* pPopulation);
	int getData();
};


class OutSumWomenAge7 : public repast::TDataSource<int>{
private:
	repast::SharedContext<Agent>* mpPopulation;

public:
	OutSumWomenAge7(repast::SharedContext<Agent>* pPopulation);
	int getData();
};


class OutSumWomenAge8 : public repast::TDataSource<int>{
private:
	repast::SharedContext<Agent>* mpPopulation;

public:
	OutSumWomenAge8(repast::SharedContext<Agent>* pPopulation);
	int getData();
};


// Total drinkers in each age * sex group
class OutTotalDrinkersMenAge0 : public repast::TDataSource<int>{
private:
	repast::SharedContext<Agent>* mpPopulation;

public:
	OutTotalDrinkersMenAge0(repast::SharedContext<Agent>* pPopulation);
	int getData();
};

class OutTotalDrinkersMenAge1 : public repast::TDataSource<int>{
private:
	repast::SharedContext<Agent>* mpPopulation;

public:
	OutTotalDrinkersMenAge1(repast::SharedContext<Agent>* pPopulation);
	int getData();
};

class OutTotalDrinkersMenAge2 : public repast::TDataSource<int>{
private:
	repast::SharedContext<Agent>* mpPopulation;

public:
	OutTotalDrinkersMenAge2(repast::SharedContext<Agent>* pPopulation);
	int getData();
};


class OutTotalDrinkersMenAge3 : public repast::TDataSource<int>{
private:
	repast::SharedContext<Agent>* mpPopulation;

public:
	OutTotalDrinkersMenAge3(repast::SharedContext<Agent>* pPopulation);
	int getData();
};


class OutTotalDrinkersMenAge4 : public repast::TDataSource<int>{
private:
	repast::SharedContext<Agent>* mpPopulation;

public:
	OutTotalDrinkersMenAge4(repast::SharedContext<Agent>* pPopulation);
	int getData();
};


class OutTotalDrinkersMenAge5 : public repast::TDataSource<int>{
private:
	repast::SharedContext<Agent>* mpPopulation;

public:
	OutTotalDrinkersMenAge5(repast::SharedContext<Agent>* pPopulation);
	int getData();
};


class OutTotalDrinkersMenAge6 : public repast::TDataSource<int>{
private:
	repast::SharedContext<Agent>* mpPopulation;

public:
	OutTotalDrinkersMenAge6(repast::SharedContext<Agent>* pPopulation);
	int getData();
};


class OutTotalDrinkersMenAge7 : public repast::TDataSource<int>{
private:
	repast::SharedContext<Agent>* mpPopulation;

public:
	OutTotalDrinkersMenAge7(repast::SharedContext<Agent>* pPopulation);
	int getData();
};


class OutTotalDrinkersMenAge8 : public repast::TDataSource<int>{
private:
	repast::SharedContext<Agent>* mpPopulation;

public:
	OutTotalDrinkersMenAge8(repast::SharedContext<Agent>* pPopulation);
	int getData();
};


class OutTotalDrinkersWomenAge0 : public repast::TDataSource<int>{
private:
	repast::SharedContext<Agent>* mpPopulation;

public:
	OutTotalDrinkersWomenAge0(repast::SharedContext<Agent>* pPopulation);
	int getData();
};


class OutTotalDrinkersWomenAge1 : public repast::TDataSource<int>{
private:
	repast::SharedContext<Agent>* mpPopulation;

public:
	OutTotalDrinkersWomenAge1(repast::SharedContext<Agent>* pPopulation);
	int getData();
};


class OutTotalDrinkersWomenAge2 : public repast::TDataSource<int>{
private:
	repast::SharedContext<Agent>* mpPopulation;

public:
	OutTotalDrinkersWomenAge2(repast::SharedContext<Agent>* pPopulation);
	int getData();
};


class OutTotalDrinkersWomenAge3 : public repast::TDataSource<int>{
private:
	repast::SharedContext<Agent>* mpPopulation;

public:
	OutTotalDrinkersWomenAge3(repast::SharedContext<Agent>* pPopulation);
	int getData();
};


class OutTotalDrinkersWomenAge4 : public repast::TDataSource<int>{
private:
	repast::SharedContext<Agent>* mpPopulation;

public:
	OutTotalDrinkersWomenAge4(repast::SharedContext<Agent>* pPopulation);
	int getData();
};


class OutTotalDrinkersWomenAge5 : public repast::TDataSource<int>{
private:
	repast::SharedContext<Agent>* mpPopulation;

public:
	OutTotalDrinkersWomenAge5(repast::SharedContext<Agent>* pPopulation);
	int getData();
};


class OutTotalDrinkersWomenAge6 : public repast::TDataSource<int>{
private:
	repast::SharedContext<Agent>* mpPopulation;

public:
	OutTotalDrinkersWomenAge6(repast::SharedContext<Agent>* pPopulation);
	int getData();
};


class OutTotalDrinkersWomenAge7 : public repast::TDataSource<int>{
private:
	repast::SharedContext<Agent>* mpPopulation;

public:
	OutTotalDrinkersWomenAge7(repast::SharedContext<Agent>* pPopulation);
	int getData();
};


class OutTotalDrinkersWomenAge8 : public repast::TDataSource<int>{
private:
	repast::SharedContext<Agent>* mpPopulation;

public:
	OutTotalDrinkersWomenAge8(repast::SharedContext<Agent>* pPopulation);
	int getData();
};

// Output number of drinks per agent
class OutSumDrinkMenAge0 : public repast::TDataSource<int>{
private:
	repast::SharedContext<Agent>* mpPopulation;

public:
	OutSumDrinkMenAge0(repast::SharedContext<Agent>* pPopulation);
	int getData();
};

class OutSumDrinkMenAge1 : public repast::TDataSource<int>{
private:
	repast::SharedContext<Agent>* mpPopulation;

public:
	OutSumDrinkMenAge1(repast::SharedContext<Agent>* pPopulation);
	int getData();
};

class OutSumDrinkMenAge2 : public repast::TDataSource<int>{
private:
	repast::SharedContext<Agent>* mpPopulation;

public:
	OutSumDrinkMenAge2(repast::SharedContext<Agent>* pPopulation);
	int getData();
};


class OutSumDrinkMenAge3 : public repast::TDataSource<int>{
private:
	repast::SharedContext<Agent>* mpPopulation;

public:
	OutSumDrinkMenAge3(repast::SharedContext<Agent>* pPopulation);
	int getData();
};


class OutSumDrinkMenAge4 : public repast::TDataSource<int>{
private:
	repast::SharedContext<Agent>* mpPopulation;

public:
	OutSumDrinkMenAge4(repast::SharedContext<Agent>* pPopulation);
	int getData();
};


class OutSumDrinkMenAge5 : public repast::TDataSource<int>{
private:
	repast::SharedContext<Agent>* mpPopulation;

public:
	OutSumDrinkMenAge5(repast::SharedContext<Agent>* pPopulation);
	int getData();
};


class OutSumDrinkMenAge6 : public repast::TDataSource<int>{
private:
	repast::SharedContext<Agent>* mpPopulation;

public:
	OutSumDrinkMenAge6(repast::SharedContext<Agent>* pPopulation);
	int getData();
};


class OutSumDrinkMenAge7 : public repast::TDataSource<int>{
private:
	repast::SharedContext<Agent>* mpPopulation;

public:
	OutSumDrinkMenAge7(repast::SharedContext<Agent>* pPopulation);
	int getData();
};


class OutSumDrinkMenAge8 : public repast::TDataSource<int>{
private:
	repast::SharedContext<Agent>* mpPopulation;

public:
	OutSumDrinkMenAge8(repast::SharedContext<Agent>* pPopulation);
	int getData();
};


class OutSumDrinkWomenAge0 : public repast::TDataSource<int>{
private:
	repast::SharedContext<Agent>* mpPopulation;

public:
	OutSumDrinkWomenAge0(repast::SharedContext<Agent>* pPopulation);
	int getData();
};


class OutSumDrinkWomenAge1 : public repast::TDataSource<int>{
private:
	repast::SharedContext<Agent>* mpPopulation;

public:
	OutSumDrinkWomenAge1(repast::SharedContext<Agent>* pPopulation);
	int getData();
};


class OutSumDrinkWomenAge2 : public repast::TDataSource<int>{
private:
	repast::SharedContext<Agent>* mpPopulation;

public:
	OutSumDrinkWomenAge2(repast::SharedContext<Agent>* pPopulation);
	int getData();
};


class OutSumDrinkWomenAge3 : public repast::TDataSource<int>{
private:
	repast::SharedContext<Agent>* mpPopulation;

public:
	OutSumDrinkWomenAge3(repast::SharedContext<Agent>* pPopulation);
	int getData();
};


class OutSumDrinkWomenAge4 : public repast::TDataSource<int>{
private:
	repast::SharedContext<Agent>* mpPopulation;

public:
	OutSumDrinkWomenAge4(repast::SharedContext<Agent>* pPopulation);
	int getData();
};


class OutSumDrinkWomenAge5 : public repast::TDataSource<int>{
private:
	repast::SharedContext<Agent>* mpPopulation;

public:
	OutSumDrinkWomenAge5(repast::SharedContext<Agent>* pPopulation);
	int getData();
};


class OutSumDrinkWomenAge6 : public repast::TDataSource<int>{
private:
	repast::SharedContext<Agent>* mpPopulation;

public:
	OutSumDrinkWomenAge6(repast::SharedContext<Agent>* pPopulation);
	int getData();
};


class OutSumDrinkWomenAge7 : public repast::TDataSource<int>{
private:
	repast::SharedContext<Agent>* mpPopulation;

public:
	OutSumDrinkWomenAge7(repast::SharedContext<Agent>* pPopulation);
	int getData();
};


class OutSumDrinkWomenAge8 : public repast::TDataSource<int>{
private:
	repast::SharedContext<Agent>* mpPopulation;

public:
	OutSumDrinkWomenAge8(repast::SharedContext<Agent>* pPopulation);
	int getData();
};

*/

#endif /* INCLUDE_DATAOUT_H_ */
