import ec.util.*;
import ec.*;
import ec.gp.*;
import ec.gp.koza.*;
import ec.simple.*;

import java.io.PrintWriter;
import java.lang.String;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class NormProblem extends GPProblem {
    private static final long serialVersionUID = 1;

    public void setup(final EvolutionState state, final Parameter base) {
        super.setup(state, base);

        // verify our input is the right class (or subclasses from it)
        if (!(input instanceof DoubleData))
            state.output.fatal("GPData class must subclass from " + DoubleData.class,
                base.push(P_DATA), null);
        }

    //DO NOT USE THIS METHOD
    public void evaluate(final EvolutionState state,
            final Individual ind,
            final int subpopulation,
            final int threadnum){}

    public void evaluate(final EvolutionState state,
            final Individual ind,
            final int subpopulation,
            final int threadnum,
            final int folderIndex) {
        if (!ind.evaluated) {
            //get the tree
            PrintWriter writer = new PrintWriter(System.out, true);
            String cTreeString = ((GPIndividual)ind).trees[0].child.makeCTree(true, true, true);
            //System.out.println(cTreeString);
            //System.out.println("Depth "+((GPIndividual)ind).trees[0].child.depth());

            //manipulate c++ src
            String modelPath = "./Models/Model"+folderIndex;
            Path templatePath = Paths.get(modelPath, "/norms/src/NormTheory.cpp.template");
            Path cppPath = Paths.get(modelPath, "/norms/src/NormTheory.cpp");
            String newLine = "return "+ cTreeString +";\n";
            try {
                List<String> fileContent = Files.readAllLines(templatePath, StandardCharsets.UTF_8);
                for (int i = 0; i < fileContent.size(); i++) {
                    if (fileContent.get(i).equals("////GP-NORM-DISPOSITION-FUNC")) {
                        fileContent.set(i, newLine);
                        break;
                    }
                }
                Files.write(cppPath, fileContent, StandardCharsets.UTF_8);
            } catch (IOException e) {
                e.printStackTrace();
            }

            //ind.evaluated = true;
        }
    }
}
