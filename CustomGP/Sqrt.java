/*
  Copyright 2006 by Sean Luke
  Licensed under the Academic Free License version 3.0
  See the file "LICENSE" for more information
*/


import ec.*;
import ec.gp.*;
import ec.util.*;
import java.lang.Math;

public class Sqrt extends GPNode
    {

    public String toString() { return "sqrt"; }

    public int expectedChildren() { return 1; }

    public void eval(final EvolutionState state,
        final int thread,
        final GPData input,
        final ADFStack stack,
        final GPIndividual individual,
        final Problem problem)
        {
        double result;
        DoubleData rd = ((DoubleData)(input));

        // children[0].eval(state,thread,input,stack,individual,problem);
        // result = rd.x;
        //
        // children[1].eval(state,thread,input,stack,individual,problem);
        // rd.x = Math.sqrt(result * rd.x);
        }

    public String makeCTree(boolean parentMadeParens, boolean printTerminalsAsVariables, boolean useOperatorForm)
        {
        if (children.length==0)
            return (printTerminalsAsVariables ? toStringForHumans() : toStringForHumans() + "()");
        else if (children.length==1 && useOperatorForm)
            return "sqrt(" +
                children[0].makeCTree(false, printTerminalsAsVariables, useOperatorForm) +
                ")";
        else
            {
            String s = "Sqrt" + "(" + children[0].makeCTree(true, printTerminalsAsVariables, useOperatorForm);
            for(int x = 1; x < children.length;x++)
                s = s + ", " + children[x].makeCTree(true, printTerminalsAsVariables, useOperatorForm);
            return s + ")";
            }
        }

    }
