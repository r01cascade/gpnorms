import java.util.ArrayList;

import ec.EvolutionState;
import ec.Individual;
import ec.simple.SimpleStatistics;
import ec.util.*;
import java.io.*;

import ec.multiobjective.MultiObjectiveStatistics;
import ec.multiobjective.MultiObjectiveFitness;
import ec.gp.GPIndividual;

public class CascadeMultiObjectiveStatistics extends MultiObjectiveStatistics {
	/** Logs the best individual of the run. */
    public void finalStatistics(final EvolutionState state, final int result)
        {
        bypassFinalStatistics(state, result);  // just call super.super.finalStatistics(...)

        if (doFinal) state.output.println("\n\n\n PARETO FRONTS", statisticslog);
        for (int s = 0; s < state.population.subpops.size(); s++)
            {
            if (doFinal) state.output.println("\n\nPareto Front of Subpopulation " + s, statisticslog);

            // build front
            ArrayList<Individual> sortedFront = MultiObjectiveFitness.getSortedParetoFront(state.population.subpops.get(s).individuals);
                        
            // print out front to statistics log
            if (doFinal)
                for (int i = 0; i < sortedFront.size(); i++)
                    ((Individual)(sortedFront.get(i))).printIndividualForHumans(state, statisticslog);
                
            // write short version of front out to disk
            if (!silentFront)
                {
                if (state.population.subpops.size() > 1)
                    state.output.println("Subpopulation " + s, frontLog);
                for (int i = 0; i < sortedFront.size(); i++)
                    {
                    Individual ind = (Individual)(sortedFront.get(i));
                    MultiObjectiveFitness mof = (MultiObjectiveFitness) (ind.fitness);
                    double[] objectives = mof.getObjectives();
        
                    String line = "";
                    for (int f = 0; f < objectives.length; f++)
                        line += (objectives[f] + ",");
                    line += ((GPIndividual)ind).trees[0].child.makeCTree(true, true, true);
                    state.output.println(line, frontLog);
                    }
                }
            }
        }
}