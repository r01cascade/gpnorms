import ec.*;
import ec.util.*;

import ec.breed.MultiBreedingPipeline;
import java.util.ArrayList;
import java.util.HashMap;
import ec.gp.GPIndividual;
import java.util.Collections;

//Do multi breeding but check for unique afterward, merged fom UniquePipeline
public class CascadeUniqueMultiBreedingPipeline extends MultiBreedingPipeline {

	public static final String P_RETRIES = "duplicate-retries";
	public int numDuplicateRetries;

	//set up: add read numDuplicateRetries variable;
	public void setup(final EvolutionState state, final Parameter base) {
        super.setup(state,base);
        Parameter def = defaultBase();
 
        // How often do we retry if we find a duplicate?
        numDuplicateRetries = state.parameters.getInt(
            base.push(P_RETRIES),def.push(P_RETRIES),0);
        if (numDuplicateRetries < 0) state.output.fatal(
            "The number of retries for duplicates must be an integer >= 0.\n",
            base.push(P_RETRIES),def.push(P_RETRIES));
    }
	
    public void prepareToProduce(
	    final EvolutionState state,
	    final int subpopulation,
	    final int thread) {

	    //System.out.println("PREPARE PRODUCE");
	    //printDebugPopulation( state.population.subpops.get(subpopulation).individuals );
    }

    public void printDebugPopulation(ArrayList<Individual> inds) {
	    for(int i = 0; i < inds.size(); i++) {
	    	if (inds.get(i)==null)
	    		System.out.printf("%d\tnull\n",i);
	        else System.out.printf("%d\t%s\n",i,((GPIndividual)inds.get(i)).trees[0].child.makeCTree(true, true, true));
	    }
    }

	int removeDuplicates(ArrayList<Individual> inds, int start, int num, final EvolutionState state, final int subpopulation) {
		ArrayList<Individual> originalSet = state.population.subpops.get(subpopulation).individuals;

		//debug print
		/*boolean removeFlag = false;
		for(int i = start; i < start + num; i++) {
			for (Individual compareInd : originalSet) {
		        if (compareInd.equals(inds.get(i)))
		        {
		        	removeFlag = true;
		        	System.out.printf("BEFORE REMOVE %d %d\n",start,num);
		        	printDebugPopulation(inds);
		        	break;
		        }
		        if (removeFlag)
		        	break;
		    }
        }*/

        //remove duplicates
    	for(int i = start; i < start + num; i++) {
    		boolean tempFlag = false;
    		for (Individual compareInd : originalSet) {
		        if (compareInd.equals(inds.get(i)))
		        {
		        	tempFlag = true;
		            inds.set(i, inds.get(start+num - 1));
		            inds.set(start+num-1, null);
		            num--;
		            i--;  // try again
		        }
			    if (tempFlag)
			    	break;
		    }
        }

        //remove null
        inds.removeAll(Collections.singleton(null));

        //debug print
        /*if (removeFlag) {
	        System.out.println("AFTER REMOVE");
		    printDebugPopulation(inds);
		}*/
    	return num;
    }

    //process from UniquePipeline but replace source with MultiBreedPipeline approach
	public int produce(final int min,
        final int max,
        final int subpopulation,
        final ArrayList<Individual> inds,
        final EvolutionState state,
        final int thread, HashMap<String, Object> misc) {

		//System.out.printf("BEFORE PRODUCE %d %d\n",min,max);
	    printDebugPopulation(inds);
		int start = 0;
        int n = 0;  // unique individuals we've built so far
        int remainder = (generateMax ? max : min);
        for(int retry = 0; retry < numDuplicateRetries + 1; retry++) {
            // grab individuals from our source and stick 'em right into inds.
            // we'll verify them from there
            int newmin = Math.min(Math.max(min - n, 1), max - n);

            BreedingSource s = sources[BreedingSource.pickRandom(sources,state.random[thread].nextDouble())];
            int num = s.produce(newmin,max - n,subpopulation,inds, state,thread, misc);
            
            int total = removeDuplicates(inds, start + n, num, state, subpopulation);  // unique individuals out of the num
            n += total;  // we'll keep those
            //if (retry>0) System.out.println("Retry " + retry);
        }
        
        if (n < remainder) { // never succeeded to build unique individuals, just make some non-unique ones
            BreedingSource s = sources[BreedingSource.pickRandom(sources,state.random[thread].nextDouble())];
            n += s.produce(remainder - n,max - n,subpopulation,inds, state,thread, misc);
            System.out.println("No more retry " + numDuplicateRetries);
        }
        //System.out.println("AFTER PRODUCE");
	    //printDebugPopulation(inds);
        return n;
    }
}
