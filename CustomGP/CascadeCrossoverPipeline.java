import ec.*;
import ec.util.*;
import ec.gp.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class CascadeCrossoverPipeline extends ec.gp.koza.CrossoverPipeline {
    public CascadeCrossoverPipeline()
    {
    parents = new ArrayList<Individual>();
    }

    public int produce(final int min,
        final int max,
        final int subpopulation,
        final ArrayList<Individual> inds,
        final EvolutionState state,
        final int thread, HashMap<String, Object> misc)

        {
        int start = inds.size();
                
        // how many individuals should we make?
        int n = typicalIndsProduced();
        if (n < min) n = min;
        if (n > max) n = max;


        // should we bother?
        if (!state.random[thread].nextBoolean(likelihood))
            {
            // just load from source 0 and clone 'em
            sources[0].produce(n,n,subpopulation,inds, state,thread,misc);
            return n;
            }

        IntBag[] parentparents = null;
        IntBag[] preserveParents = null;
        if (misc!=null&&misc.get(KEY_PARENTS) != null)
            {
            preserveParents = (IntBag[])misc.get(KEY_PARENTS);
            parentparents = new IntBag[2];
            misc.put(KEY_PARENTS, parentparents);
            }

        GPInitializer initializer = ((GPInitializer)state.initializer);
        

        for(int q=start;q<n+start; /* no increment */)  // keep on going until we're filled up
            {
            int sameParentRetries = 0;
            boolean sameFlag;
            do {
                sameFlag = false;
                parents.clear();
                
                // grab two individuals from our sources
                if (sources[0]==sources[1])  // grab from the same source
                    sources[0].produce(2,2,subpopulation, parents, state,thread, misc);
                else // grab from different sources
                    {
                    sources[0].produce(1,1,subpopulation, parents, state,thread, misc);
                    sources[1].produce(1,1,subpopulation, parents, state,thread, misc);
                    }
                
                // at this point, parents[] contains our two selected individuals
                
                // are our tree values valid?
                if (tree1!=TREE_UNFIXED && (tree1<0 || tree1 >= ((GPIndividual)(parents.get(0))).trees.length))
                    // uh oh
                    state.output.fatal("GP Crossover Pipeline attempted to fix tree.0 to a value which was out of bounds of the array of the individual's trees.  Check the pipeline's fixed tree values -- they may be negative or greater than the number of trees in an individual"); 
                if (tree2!=TREE_UNFIXED && (tree2<0 || tree2 >= ((GPIndividual)(parents.get(1))).trees.length))
                    // uh oh
                    state.output.fatal("GP Crossover Pipeline attempted to fix tree.1 to a value which was out of bounds of the array of the individual's trees.  Check the pipeline's fixed tree values -- they may be negative or greater than the number of trees in an individual"); 

                int t1=0; int t2=0;
                if (tree1==TREE_UNFIXED || tree2==TREE_UNFIXED) 
                    {
                    do
                        // pick random trees  -- their GPTreeConstraints must be the same
                        {
                        if (tree1==TREE_UNFIXED) 
                            if (((GPIndividual)(parents.get(0))).trees.length > 1)
                                t1 = state.random[thread].nextInt(((GPIndividual)(parents.get(0))).trees.length);
                            else t1 = 0;
                        else t1 = tree1;

                        if (tree2==TREE_UNFIXED) 
                            if (((GPIndividual)(parents.get(1))).trees.length>1)
                                t2 = state.random[thread].nextInt(((GPIndividual)(parents.get(1))).trees.length);
                            else t2 = 0;
                        else t2 = tree2;
                        } while (((GPIndividual)(parents.get(0))).trees[t1].constraints(initializer) != ((GPIndividual)(parents.get(1))).trees[t2].constraints(initializer));
                    }
                else
                    {
                    t1 = tree1;
                    t2 = tree2;
                    // make sure the constraints are okay
                    if (((GPIndividual)(parents.get(0))).trees[t1].constraints(initializer)
                        != ((GPIndividual)(parents.get(1))).trees[t2].constraints(initializer)) // uh oh
                        state.output.fatal("GP Crossover Pipeline's two tree choices are both specified by the user -- but their GPTreeConstraints are not the same");
                    }



                // validity results...
                boolean res1 = false;
                boolean res2 = false;
                
                
                // prepare the nodeselectors
                nodeselect1.reset();
                nodeselect2.reset();
                
                
                // pick some nodes
                
                GPNode p1=null;
                GPNode p2=null;
                
                for(int x=0;x<numTries;x++)
                    {
                    // pick a node in individual 1
                    p1 = nodeselect1.pickNode(state,subpopulation,thread,((GPIndividual)(parents.get(0))),((GPIndividual)(parents.get(0))).trees[t1]);
                    
                    // pick a node in individual 2
                    p2 = nodeselect2.pickNode(state,subpopulation,thread,((GPIndividual)(parents.get(1))),((GPIndividual)(parents.get(1))).trees[t2]);
                    
                    // check for depth and swap-compatibility limits
                    res1 = verifyPoints(initializer,p2,p1);  // p2 can fill p1's spot -- order is important!
                    if (n-(q-start)<2 || tossSecondParent) res2 = true;
                    else res2 = verifyPoints(initializer,p1,p2);  // p1 can fill p2's spot -- order is important!
                    
                    // did we get something that had both nodes verified?
                    // we reject if EITHER of them is invalid.  This is what lil-gp does.
                    // Koza only has numTries set to 1, so it's compatible as well.
                    if (res1 && res2) break;
                    }

                // at this point, res1 AND res2 are valid, OR either res1
                // OR res2 is valid and we ran out of tries, OR neither is
                // valid and we ran out of tries.  So now we will transfer
                // to a tree which has res1 or res2 valid, otherwise it'll
                // just get replicated.  This is compatible with both Koza
                // and lil-gp.
                

                // at this point I could check to see if my sources were breeding
                // pipelines -- but I'm too lazy to write that code (it's a little
                // complicated) to just swap one individual over or both over,
                // -- it might still entail some copying.  Perhaps in the future.
                // It would make things faster perhaps, not requiring all that
                // cloning.

                
                
                // Create some new individuals based on the old ones -- since
                // GPTree doesn't deep-clone, this should be just fine.  Perhaps we
                // should change this to proto off of the main species prototype, but
                // we have to then copy so much stuff over; it's not worth it.
                        
                GPIndividual j1 = (GPIndividual)(((GPIndividual)(parents.get(0))).lightClone());
                GPIndividual j2 = null;
                if (n-(q-start)>=2 && !tossSecondParent) j2 = (GPIndividual)(((GPIndividual)(parents.get(1))).lightClone());
                
                // Fill in various tree information that didn't get filled in there
                j1.trees = new GPTree[((GPIndividual)(parents.get(0))).trees.length];
                if (n-(q-start)>=2 && !tossSecondParent) j2.trees = new GPTree[((GPIndividual)(parents.get(1))).trees.length];
                
                // at this point, p1 or p2, or both, may be null.
                // If not, swap one in.  Else just copy the parent.
                
                for(int x=0;x<j1.trees.length;x++)
                    {
                    if (x==t1 && res1)  // we've got a tree with a kicking cross position!
                        { 
                        j1.trees[x] = (GPTree)(((GPIndividual)(parents.get(0))).trees[x].lightClone());
                        j1.trees[x].owner = j1;
                        j1.trees[x].child = ((GPIndividual)(parents.get(0))).trees[x].child.cloneReplacing(p2,p1);
                        j1.trees[x].child.parent = j1.trees[x];
                        j1.trees[x].child.argposition = 0;
                        j1.evaluated = false; 
                        }  // it's changed
                    else 
                        {
                        j1.trees[x] = (GPTree)(((GPIndividual)(parents.get(0))).trees[x].lightClone());
                        j1.trees[x].owner = j1;
                        j1.trees[x].child = (GPNode)(((GPIndividual)(parents.get(0))).trees[x].child.clone());
                        j1.trees[x].child.parent = j1.trees[x];
                        j1.trees[x].child.argposition = 0;
                        }
                    }
                
                if (n-(q-start)>=2 && !tossSecondParent) 
                    for(int x=0;x<j2.trees.length;x++)
                        {
                        if (x==t2 && res2)  // we've got a tree with a kicking cross position!
                            { 
                            j2.trees[x] = (GPTree)(((GPIndividual)(parents.get(1))).trees[x].lightClone());
                            j2.trees[x].owner = j2;
                            j2.trees[x].child = ((GPIndividual)(parents.get(1))).trees[x].child.cloneReplacing(p1,p2);
                            j2.trees[x].child.parent = j2.trees[x];
                            j2.trees[x].child.argposition = 0;
                            j2.evaluated = false; 
                            } // it's changed
                        else 
                            {
                            j2.trees[x] = (GPTree)(((GPIndividual)(parents.get(1))).trees[x].lightClone());
                            j2.trees[x].owner = j2;
                            j2.trees[x].child = (GPNode)(((GPIndividual)(parents.get(1))).trees[x].child.clone());
                            j2.trees[x].child.parent = j2.trees[x];
                            j2.trees[x].child.argposition = 0;
                            }
                        }

                //CASCADE - check if children are the same as parents
                if (j1==null || j2==null) {
                    System.out.printf("---xover---\n%s\n%s\nnull\nnull\n---end---\n",
                        ((GPIndividual)(parents.get(0))).trees[0].child.makeCTree(true, true, true),
                        ((GPIndividual)(parents.get(1))).trees[0].child.makeCTree(true, true, true)
                    );
                } else {
                    System.out.printf("---xover---\n%s\n%s\n%s\n%s\n---end---\n",
                        ((GPIndividual)(parents.get(0))).trees[0].child.makeCTree(true, true, true),
                        ((GPIndividual)(parents.get(1))).trees[0].child.makeCTree(true, true, true),
                        j1.trees[0].child.makeCTree(true, true, true),
                        j2.trees[0].child.makeCTree(true, true, true)
                    );
                    //clone if childrens equal parents, thus no re-evaluated
                    GPIndividual parent1 = ((GPIndividual)(parents.get(0)));
                    GPIndividual parent2 = ((GPIndividual)(parents.get(1)));
                    if ( (j1.equals(parent1) && j2.equals(parent2)) || (j2.equals(parent1) && j1.equals(parent2)) ) {
                        System.out.println("Same.");
                        sameFlag = true;
                        sameParentRetries++;
                        // just load from source 0 and clone 'em
                        //sources[0].produce(n,n,subpopulation,inds, state,thread,misc);
                        //return n;
                    }
                }

                if (!sameFlag) {
                	// add the individuals to the population
                	//inds.set(q, j1);
                	inds.add(j1);
                	if (preserveParents != null)
                    	{
                    	parentparents[0].addAll(parentparents[1]);
                    	preserveParents[q] = parentparents[0];
                    	}
                	q++;
                	if (q<n+start && !tossSecondParent)
                    	{
                    	//inds.set(q, j2);
                    	inds.add(j2);
                    	if (preserveParents != null)
                        	{
                        	parentparents[0].addAll(parentparents[1]);
                        	preserveParents[q] = parentparents[0];
                        	}
                    	q++;
                    	}
                }

                } while (sameFlag && sameParentRetries < numTries);
            }            
        return n;
        }
    }

