import ec.simple.SimpleStatistics;
import ec.simple.SimpleProblemForm;
import java.util.Collections;
import java.util.Comparator;
import ec.gp.koza.KozaFitness;

import ec.*;
import ec.steadystate.*;

public class CustomStatistics extends SimpleStatistics implements SteadyStateStatisticsForm
    {
    
    /** Overwrite SimpleStatistics. Logs the top 5 individual of the run. */
    public void finalStatistics(final EvolutionState state, final int result)
        {
        super.finalStatistics(state,result);
        
        // for now we just print the best fitness 
        
        if (doFinal) state.output.println("\nBest Individual of Run:",statisticslog);
        for(int x = 0; x< state.population.subpops.size(); x++ )
            {
            if (doFinal) state.output.println("Subpopulation " + x + ":",statisticslog);
            if (doFinal) best_of_run[x].printIndividualForHumans(state,statisticslog);
            
            if (doMessage && !silentPrint) state.output.message("Subpop " + x + " best fitness of run: " + best_of_run[x].fitness.fitnessToStringForHumans());
            // finally describe the winner if there is a description
            if (doFinal && doDescription) 
                if (state.evaluator.p_problem instanceof SimpleProblemForm)
                    ((SimpleProblemForm)(state.evaluator.p_problem.clone())).describe(state, best_of_run[x], x, 0, statisticslog);      

            //sort by fitness, ascending order
            Collections.sort(state.population.subpops.get(x).individuals,
            new Comparator<Individual>() {
                public int compare(Individual a, Individual b) {
                    if (a.fitness.betterThan(b.fitness))
                        return -1;
                    if (b.fitness.betterThan(a.fitness))
                        return 1;
                    return 0;
                    }
                });
            
            //print top 5
            state.output.println("",statisticslog);
            if (doFinal) {
                state.output.println("=== Top 5 for subpopulation " + x + " ===",statisticslog);
                for (int i=0; i<5; i++) {
                    Individual ind = state.population.subpops.get(x).individuals.get(i);
                    ind.printIndividualForHumans(state,statisticslog);
                }
            }

            }
        }
    }
