import java.util.*;
import java.io.*;
import java.lang.reflect.Array;

import ec.util.*;

import ec.*;
import ec.gp.GPIndividual;

public class CascadeSubpopulation extends Subpopulation
    {
    	//override to check for depth > 1 during retries. So will be affected by re-tries parameter
    	public void populate(EvolutionState state, int thread)
        {
        int len = initialSize;                         // original length of individual ArrayList
        int start = 0;                          // where to start filling new individuals in -- may get modified if we read some individuals in
        
        // should we load individuals from a file? -- duplicates are permitted
        if (loadInds)
            {
            InputStream stream = state.parameters.getResource(file,null);
            if (stream == null)
                state.output.fatal("Could not load subpopulation from file", file);
            
            try { readSubpopulation(state, new LineNumberReader(new InputStreamReader(stream))); }
            catch (IOException e) { state.output.fatal("An IOException occurred when trying to read from the file " + state.parameters.getString(file, null) + ".  The IOException was: \n" + e,
                    file, null); }
            
            if (len < individuals.size())
                {
                state.output.message("Old subpopulation was of size " + len + ", expanding to size " + individuals.size());
                return;
                }
            else if (len > individuals.size())   // the population was shrunk, there's more space yet
                {
                // What do we do with the remainder?
                if (extraBehavior == TRUNCATE)
                    {
                    state.output.message("Old subpopulation was of size " + len + ", truncating to size " + individuals.size());
                    return;  // we're done
                    }
                else if (extraBehavior == WRAP)
                    {
                    state.output.message("Only " + individuals.size() + " individuals were read in.  Subpopulation will stay size " + len + 
                        ", and the rest will be filled with copies of the read-in individuals.");
                    start = individuals.size();
                    int count = 0;
                    for(int i = start; i < len; ++i)
                        {
                        individuals.add((Individual) individuals.get(count).clone());
                        if(++count >= start) count = 0;
                        }
                    return;
                    }
                else // if (extraBehavior == FILL)
                    {
                    state.output.message("Only " + individuals.size() + " individuals were read in.  Subpopulation will stay size " + len + 
                        ", and the rest will be filled using randomly generated individuals.");
                    
                    // mark the start position for filling in
                    start = individuals.size();
                    // now go on to fill the rest below...
                    }                       
                }
            else // exactly right number, we're done
                {
                return;
                }
            }

        // populating the remainder with random individuals
        HashMap h = null;
        if (numDuplicateRetries >= 1)
            h = new HashMap((len - start) / 2);  // seems reasonable

        for(int x=start;x<len;x++) 
            {
            Individual newInd = null;
            for(int tries=0; 
                tries <= /* Yes, I see that*/ numDuplicateRetries; 
                tries++)
                {
                newInd = species.newIndividual(state, thread);
                if ( ((GPIndividual)newInd).trees[0].child.depth() > 1 ) //For CASCADE: depth > 1
	                {
	                if (numDuplicateRetries >= 1)
	                    {
	                    // check for duplicates
	                    Object o = h.get(newInd);
	                    if (o == null) // found nothing, we're safe
	                        // hash it and go
	                        {
	                        h.put(newInd,newInd);
	                        break;
	                        }
	                    }
	                }
                }  // oh well, we tried to cut down the duplicates
            individuals.add(newInd);
            }
        }
    }