import ec.simple.SimpleEvaluator;
import java.util.ArrayList;
import java.util.Arrays;

import ec.*;
import ec.util.*;
import ec.simple.SimpleProblemForm;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import java.lang.ProcessBuilder;
import java.lang.Process;

import java.util.Scanner;
import ec.multiobjective.MultiObjectiveFitness;
import ec.gp.GPIndividual;

public class CascadeEvaluator extends SimpleEvaluator {

	Object[] lock = new Object[0];          // Arrays are serializable

    protected void evalPopChunk(EvolutionState state, int[] numinds, int[] from, int threadnum, SimpleProblemForm p) {
	    ((ec.Problem)p).prepareToEvaluate(state,threadnum);
	    
	    ArrayList<Subpopulation> subpops = state.population.subpops;
	    int len = subpops.size();
	    
	    for(int pop=0;pop<len;pop++) {
	    	synchronized(lock) {
		        // start evaluatin'! => ONLY modify source codes
		        int fp = from[pop];
		        int upperbound = fp+numinds[pop];
		        ArrayList<Individual> inds = subpops.get(pop).individuals;
		        System.out.print("Edit files: ");
			boolean evaluateFlag = false;
		        for (int x=fp;x<upperbound;x++) {
		        	if (!inds.get(x).evaluated) {
		        		System.out.print((x-fp)+"\t");
					evaluateFlag = true;
				}
				//evaluated of ind will be check in NormProblem so don't have to do it here
				((NormProblem)p).evaluate(state,inds.get(x), pop, threadnum, x-fp); //IMPORTANT: folderIndex = x-fp
		        }
		        System.out.println();
		        state.incrementEvaluations(upperbound - fp);
		        
		        //run c++ model
		        //TO DO: only run the one that is not evaluated
			if (evaluateFlag) {
	            		try {
	            			System.out.printf("Running inds: %d to %d\n",fp,upperbound-1);
	                		Process process = new ProcessBuilder("./runModels.sh",Integer.toString(upperbound-fp)).start();
	                		process.waitFor();
	            		} catch (IOException e) {
	                		e.printStackTrace();
	            		} catch (InterruptedException e) {
	                		e.printStackTrace();
	            		}
			}
	            //read the fitness
	            System.out.println("Read fitness: ");
	            for (int x=fp;x<upperbound;x++) {
	            	if (!inds.get(x).evaluated) {
	            		System.out.print((x-fp)+"\t");
			            double implausabilityError = 0.0;
			            Path fitnessPath = Paths.get("./Models/Model"+(x-fp)+"/fitness.out"); //IMPORTANT: folderIndex = x-fp
			            try {
			                List<String> fileContent = Files.readAllLines(fitnessPath, StandardCharsets.UTF_8);
			                String firstLine = fileContent.get(0);
			                if (firstLine.equals("inf") || firstLine.equals("-inf") || firstLine.equals("nan")) {
			                    //error => set fitness to infinity
			                    implausabilityError = Double.MAX_VALUE;
			                } else {
			                    implausabilityError = Double.parseDouble(firstLine);
			                }
			            } catch (IOException e) {
			                System.out.println(e);
			            }
			            double[] objectives = ((MultiObjectiveFitness)inds.get(x).fitness).getObjectives();
			            objectives[0]=implausabilityError;
			            objectives[1]=inds.get(x).size();
			            ((MultiObjectiveFitness)inds.get(x).fitness).setObjectives(state, objectives);

				    	String cTreeString = ((GPIndividual)inds.get(x)).trees[0].child.makeCTree(true, true, true);
				    	System.out.printf("==%d==\t%d\t[%.3f\t%.3f]\t%s\n",
							x,
							((GPIndividual)inds.get(x)).trees[0].child.depth(),
							objectives[0],objectives[1],
							cTreeString
						);

						inds.get(x).evaluated = true;
					}
		        }
		        System.out.println();
		    }
	    }
	                    
	    ((ec.Problem)p).finishEvaluating(state,threadnum);
    }
}
