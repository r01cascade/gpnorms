clean:
	rm -f CustomGP/*.class
	rm -f *.stat
	rm -f *.gz
	rm -f Models/Model*/fitness.out

compileJ:
	javac -classpath /home/cascade/sfw/ecj/target/ecj-26.jar -O CustomGP/*.java -d CustomGP

runJ:
	java -classpath ./CustomGP:/home/cascade/sfw/ecj/target/ecj-26.jar ec.Evolve -file CustomGP/norm.params

all: clean compileJ runJ
